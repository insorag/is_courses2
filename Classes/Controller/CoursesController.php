<?php
namespace INSOR\IsCourses2\Controller;

/***************************************************************
*
*  Copyright notice
*
*  (c) 2016
*
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 3 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
use INSOR\IsCourses2\Helper\InsorLib;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
* CoursesController
*/
class CoursesController extends ActionController
{

  /**
  * coursesRepository
  * 
  * @var \INSOR\IsCourses2\Domain\Repository\CoursesRepository
  * @inject
  */
  protected $coursesRepository = NULL;

  /**
   * teachersRepository
   *
   * @var \INSOR\IsCourses2\Domain\Repository\TeachersRepository
   * @inject
   */
  protected $teachersRepository = NULL;

  /**
   * Aktuell eingeloggter FE-User
   *
   * @var null
   */
  private $feuser_uid = null;

  /**
   * Alle Kurse des FE-Users
   *
   * @var array
   */
  private $teachercourses = array();

  // TODO: Neu den encryptionKey aus LocalConfiguration verwenden
  private $secret = "549af0f6-3117-4ad6-b7e7-0f547d67ffe1";

  /**
   * Init Funktion um Kundenobjekt zu instanzieren
   */
  private function initFeuser()
  {
    $GLOBALS['TSFE']->connectToDB();
    $GLOBALS['TSFE']->initFEuser();

    $this->feuser_uid = intval($GLOBALS['TSFE']->fe_user->user['uid']);

    if ($this->feuser_uid) {
      $this->teachercourses = $this->coursesRepository->getCoursesByOwner($this->feuser_uid);
    }
  }

  /**
   * Rekursive Funktion, um eine Kategorie mit deren Unterkategorien auszulesen
   *
   * @param $parent
   * @param array $cats
   * @return array
   */
  private function getCategoryChildrenRecursive($parent, &$cats = array()) {
    $children = $this->coursesRepository->findCategories($parent);
    if ($children) {
      foreach ($children as $row) {
        $keys[] = $row['uid'];
        $cats[] = $row['uid'];
      }
    }
    // TODO: FKA HOTFIX
    if ($keys && count($keys)) {
      foreach($keys as $catkey) {
        $this->getCategoryChildrenRecursive($catkey, $cats);
      }
      return $cats;
    } else {
      return array();
    }
  }

  /**
   * Listet alle Kurse auf, allenfalls eingeschränkt über Settings oder Args (UID-Liste)
   *
   * @return void
   */
  public function listAction()
  {
    $this->getAndAssignCourses();
  }

  private function getAndAssignCourses(&$courses = null, $includeStatsRecords = false, $onlyFilledStats = false) {
    $args = $this->request->getArguments();

    // Kurssprachen auslesen falls gewünscht
    $sys_language_uid = ($this->settings['showAllLanguages'] == 0) ? $GLOBALS['TSFE']->sys_language_uid : false;

    // Nur künftige Kurse anzeigen
    $onlyFutureCourses = true;

    $this->initFeuser();

    // Falls TypoScript nicht eingebunden über den Include, dann Fehlermeldung anzeigen.
    if (!$this->settings) {
      $this->addFlashMessage('Keine Konfiguration gefunden. Extension-Typoscript Include vorhanden?', '', AbstractMessage::ERROR);
      return;
    }

    // Nur eigene Kurse anzeigen (dann auch hidden Kurse)
    if ($this->settings['showOnlyOwn']) {
      $onlyFromThisFeuser = $this->feuser_uid ?: -1;
      $includeHiddenCourses = true;
    }

    // Kategorien Filter auslesen
    $categs = $this->settings['filterCategories'];

    // Alle Unterkategorien holen, und als neue Cat-Liste verwenden
    if ($categs) {
      $cats = explode(',', $categs);
      foreach ($cats as $categ) {
        $cats = array_merge($this->getCategoryChildrenRecursive($categ), $cats);
      }
      $cats = array_unique($cats);
      $categs = implode(',', $cats);
    }

    // Kategorien AND verknüpfen
    $categFilterAnd = $this->settings['categFilterAnd'];

    // Maximale Anzahl anzuzeigender Einträge auslesen
    $limit = $this->settings['nofEntries'];

    // Nur buchbare Kurse anzeigen?
    $onlyBookable = $this->settings['showOnlyBookable'];

    // Anzeige auf gewisse UIDs einschränken?
    if ($args['course_uids']) {
      $course_uids = implode(',', array_map('intval', explode(',', $args['course_uids'])));
      $onlyFutureCourses = false;
    }

    // Auch vergangene Kurse anzeigen?
    if ($args['show_past_courses']) {
      $onlyFutureCourses = false;
    }

    // Nur aus einem bestimmten Jahr anzeigen
    //$onlyYear = $args['year'] ?: null;
    $onlyYear = $this->settings['showYear'];
    if ($onlyYear) {
      $onlyFutureCourses = false;
    }

    // Alle Kurse holen
    $courses = $this->coursesRepository->findAll($categs, $limit, $onlyBookable, $onlyFutureCourses, $onlyFromThisFeuser, $course_uids, $onlyYear, $includeHiddenCourses, $categFilterAnd, $includeStatsRecords, $onlyFilledStats, $sys_language_uid);

    // Falls keine Kurse gefunden, dann entsprechende Template Variable setzen
    if (count($courses) == 0) {
      $this->view->assign('no_results', 1);
    }

    // Ausgeben aller Kategorien Filter
    $filter = array();
    foreach ($this->coursesRepository->findCategories($this->settings['categoryFilterRootnode']) as $uid => $record) {
      $filter[] = array(
        'uid' => $uid,
        'title' => $record['title'],
        'elements' => $this->coursesRepository->findCategories($uid, true)
      );
    }

    // Welche Kategorien befinden sich alle im Resultset?
    $used_cats = array();
    foreach($courses as $course) {
      foreach(explode(',', $course['categories']) as $cat) {
        if (!in_array($cat, $used_cats)) {
          $used_cats[] = $cat;
        }
      }
    }

    // Alle Filter-Kategorien entfernen, welche im Resultset ohnehin nicht vorkommen
    foreach ($filter as $parent_key => $parent) {
      foreach ($parent['elements'] as $child_key => $child) {
        if (!in_array($child['uid'], $used_cats)) {
          unset($filter[$parent_key]['elements'][$child_key]);
        }
      }
    }

    $this->view->assign('filter', $filter);

    // SignupForm URL mit Powermail Parameter Mapping gemäss Typoscript Config
    foreach ($courses as $uid => $course) {
      $params = array();
      foreach ($this->settings['urlParameterMapping'] as $field => $param) {
        if ($param) {
          $params[$param] = $course[$field];
        }
      }
      $courses[$uid]['params'] = $params;

      // Show Action Link: Interne custom Anmeldung: Details auf eigener Page
      if (intval($course['anmelde_url'])) {
        $courses[$uid]['detailPid'] = intval($course['anmelde_url']);

        // Show Action Link: Externe custom Anmeldung: Details auf aktueller Page
      } else if ($course['anmelde_url']) {
        $courses[$uid]['detailPid'] = $GLOBALS['TSFE']->id;

        // Show Action Link: Keine Angabe: Details auf Default-Anmeldeformular Page
      } else {
        $courses[$uid]['detailPid'] = $this->settings['defaultSubscriptionFormPid'];
      }

      // Beschreibung und Titel: Tags entfernen für Listenansicht
      $courses[$uid]['titel'] = strip_tags(InsorLib::br2space($courses[$uid]['titel']));
      $courses[$uid]['beschreibung'] = strip_tags(InsorLib::br2space($courses[$uid]['beschreibung']));

      // Dem Kurs zugeordnete FE-User auslesen
      foreach ($this->coursesRepository->findCourseTeachers($uid) as $teacher_uid => $record) {
        $courses[$uid]['teachers'][$teacher_uid] = $record;
      }

      // Das Kursbild für diesen Kurs
      if ($courses[$uid]['teachers']) {
        $courses[$uid]['coursepic'] = $this->getCoursePicturePath($uid, array_values($courses[$uid]['teachers'])[0]['uid']);
      }

      // Anmeldeschluss erreicht? Flag setzen
      $courses[$uid]['anmeldeschluss'] = (($courses[$uid]['anmeldeschluss_datum'] > 0) && ($courses[$uid]['anmeldeschluss_datum'] < time())) ? 1 : 0;

    }

    // Editierlink anzeigen, falls Berechtigung zum Bearbeiten vorhanden
    if ($this->feuser_uid) {
      foreach ($courses as $uid => &$course) {
        if ($this->hasUserCourseWritePermission($uid)) {
          $course['writable'] = true;
        }
      }
    }

    // Alle Settings dem FE zuweisen, zur Verwendung im Plugin
    $this->view->assign('settings', $this->settings);

    // Kurse Daten dem Template zuweisen
    $this->view->assign('courses', $courses);

    // Contentobject Daten an Fluidview weitergeben
    $cObjectData = $this->configurationManager->getContentObject()->data['uid'];
    $this->view->assign('cuid', $cObjectData);

    // Falls API Anforderung, dann JSON ausgeben
    if ($_GET['json'] && $_GET['no_cache']) {
      $json = json_encode($courses);
      echo $json;
      exit();
    }

  }

  /**
   * Statistik Auflistung. Im Grunde genommen eine normale Liste, aber einfach mit einem anderen Template
   * für die Statistik Eingabe.
   */
  public function listStatsAction() {
    $courses = array();
    $this->getAndAssignCourses($courses, true);

    // Anreichern von Infos
    foreach($courses as $uid => $course) {

      $statsFormData = $this->getStatsFormLink($course);
      $courses[$uid]['statsFormPid'] = $statsFormData['statsFormPid'];
      $courses[$uid]['statsFormHmac'] = $statsFormData['statsFormHmac'];
      $courses[$uid]['statsParams'] = $statsFormData['statsParams'];

      // Herausfinden, ob die Statistikeingabe überfällig ist (+ 1 Tag)
      if (intval($course['end_datum'])+86400 < date('U') && $course['end_datum'] > date('U',1514764800)) {
        $courses[$uid]['statsOverdue'] = true;
        $courses[$uid]['showemail'] = true;
      }

      // Wenn Kurs versteckt, Statistik-Eintrag oder Statistik schon vergeben, keine Mail moeglich machen
      if($course['hidden'] == 1 || $course['stats_only_record'] == 1 || $course['statistik'] != '') {
        $courses[$uid]['showemail'] = false;
      } else {
        // Wenn Weder Alternativ-Email vergeben noch Teacher vergeben, keine Mail moeglich
        if($course['teachers'] === null && $course['teacher_email'] == '') {
          $courses[$uid]['showemail'] = false;
        }
      }

      // Log Anzeige: nur erste Zeile
      $courses[$uid]['log'] = explode("\n", $courses[$uid]['log'])[0];

    }

    // Kurse dem Template zuweisen
    $this->view->assign('courses', $courses);

  }

  private function getStatsFormLink($course) {
    $uid = $course['uid'];

    // Statistik Eingabemaske herausfinden, oder Default nehmen
    if ($course['owner']) {
      $feusers = $this->coursesRepository->getFeuserRootline($course['owner']);

      $statsForm = $this->coursesRepository->getConfiguredStatsForm($uid, $feusers);
      $result['statsFormPid'] = $statsForm;
    }

    $result['statsFormPid'] = ($statsForm) ?: $this->settings['defaultStatsFormPid'];
    $result['statsFormHmac'] = sha1("$uid-{$this->secret}");

    // Für die Statistik-Links, die bereits ausgefüllten Antworten anhängen als Powermail-Parameter
    $params = array();
    if ($course['statistik']) {
      $statistik = json_decode($course['statistik']);
      foreach($statistik as $field => $answer) {
        $params["tx_powermail_pi1[field][$field]"] = $answer;
      }
    }
    $result['statsParams'] = $params;
    return $result;
  }

  /**
   * Mailer-Action für Reminder, wenn auf "Mail-Symbol" in der Statistik-Liste geklickt wird.
   * Action wird nur über AJAX aufgerufen und gibt einen einzelnen String als Text aus, der interpretiert werden kann.
   *
   */
  public function mailStatsAction() {

    /**
     * @var $teacher \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
     */
    $teacher = null;

    /**
     * @var $mail \INSOR\IsCourses2\Helper\StatMailer
     */
    $mail = null;

    $param = $this->request->getArgument('course_uid');

    // Wenn keine Parameter angegeben, dann action nicht ausführen.
    if($param != '') {
      // Kurse auslesen
      $course = array_values($this->coursesRepository->findStatMailCourses(intval($param)))[0];

      //Standard-Wert: Alternative wird im Normalfall nicht gebraucht, wenn Teacher vorhanden
      if($course['course_teacher_email'] != '') {
        $isAlternativeTeacherEmail = 1;
      } else {
        $isAlternativeTeacherEmail = 0;
      }

      // Teacher abfrage gar nicht erst machen, wenn Alternativ-Email schon gegeben
      if($isAlternativeTeacherEmail == 0) {
        // FE-User darf nicht 0 sein, sonst kann kein Mail verschickt werden
        if(!$course['fe_user_id']) {
          $this->coursesRepository->writeToCourseLog (
            "Statistik-Mail Reminder zu KursID ".$course['course_uid']." wurde nicht versendet: kein Teacher zugewiesen.",
            $course['course_uid']
          );
          $this->log('statmailer_task',"Statistik-Mail zu KursID ".$course['course_uid']." wurde nicht versendet: kein Teacher zugewiesen.");
          echo "0";
          return "";
        } else {
          $teacher = $this->teachersRepository->findByUid(intval($course['fe_user_id']));
          // FE-User-ID muss effektiv auch gefunden werden
          if($teacher === FALSE) {
            $this->coursesRepository->writeToCourseLog (
              "Statistik-Mail Reminder zu KursID ".$course['course_uid']." wurde nicht versendet: TeacherID ".$course['fe_user_id']." ist inaktiv oder geloescht.",
              $course['course_uid']
            );
            echo "00";
            return "";
          } else {
            if (intval($teacher['keine_benachrichtigungen']) == 1) {
              echo "00";
              return "";
            }
          }
        }
      }

      // Statistik Eingabemaske herausfinden, oder Default nehmen
      if ($course['owner']) {
        //Fe-User Rootline auslesen (gesamte Hierarchie)
        $feusers = $this->coursesRepository->getFeuserRootline($course['owner']);

        // Abgleich mit Custom-Konfigurationstabelle, falls für diesen Kurs ein anderes Formular gilt
        $statsForm = $this->coursesRepository->getConfiguredStatsForm($course, $feusers);
        $course['statsFormPid'] = $statsForm;
      }

      // Default setzen oder Custom
      $course['statsFormPid'] = ($statsForm) ?: $this->settings['defaultStatsFormPid'];
      // HMac Security-Hash generieren.
      $course['statsFormHmac'] = sha1($course['course_uid']."-{$this->secret}");

      // StatMailer - Objekt erstellen und Mail zusammenstellen
      $mail = $this->objectManager->get('INSOR\IsCourses2\Helper\StatMailer');

      // Wenn alternativ-Email benutzt wird, diese abfuellen, ansonsten teacher
      if($isAlternativeTeacherEmail === 1) {
        $teacher_email = $course['course_teacher_email'];
        $teacher_name = ($course['course_teacher_text'] == '') ? 'Dozent' : $course['course_teacher_text'];
      } else {
        $teacher_email = $teacher['email'];
        $teacher_name = $teacher['name'];
      }

      /**
       * Siehe Parameter der Funktion fuer erleuterung.
       * lang Parameter auf de, weil vorerst alle Sprachen in 1 Template (Keine Abfrage der Kurse auf Sprache)
       */
      $mail->createMail(
        'reminder',
        'de',
        ['noreply@elternbildung.ch' => 'Elternbildung Statistik'],
        [$teacher_email => $teacher_name],
        'Elternbildung CH: bitte Kursstatistik nachführen (Erinnerung) / Formation des parents CH: saisir la statistique de votre cours (Rappel)',
        $teacher['name'],
        $course,
        [
          'webmaster@insor.ch' => 'INSOR Elternbildung Statistik-Scheduler BCC',
        ]
      );

      //'statistik@elternbildung.ch' => 'Statistik Elternbildung CH'

      // Wenn Email gesendet wurde, loggen und stempfeln, ansonsten Fehlermeldung
      $isSent = $mail->sendMail();
      if ($isSent) {
        if($isAlternativeTeacherEmail === 1) {
          $this->coursesRepository->writeToCourseLog (
            "Statistik-Mail zu KursID ".$course['course_uid'].": Alternativ-Mail verwendet; Email wurde an teacher_email: ".$course['course_teacher_email']." versendet.",
            $course['course_uid']
          );
        } else {
          $this->coursesRepository->writeToCourseLog (
            'Statistik-Mail Reminder zu KursID '.$course['course_uid'].' versendet an: '. $teacher['email'],
            $course['course_uid']
          );
        }
        $this->coursesRepository->writeStatsReminderSent(
          $course['course_uid']
        );
        echo "1";
        return "";
      } else {
        $this->coursesRepository->writeToCourseLog (
          "Statistik-Mail zu KursID ".$course['course_uid']." wurde nicht versendet: Mailing-Error",
          $course['course_uid']
        );
        echo "000";
        return "";
      }

    }
  }

  /**
   * Liest ausgefuellte Statistiken von im Flexform angegebenen Jahr aus
   *
   * Generiertes Array sieht so aus:
   * [
   *  'keys' => [
   *    'feld1' => 'feld1',
   *    'feld2' => 'feld2'
   *  ],
   *  'values' => [
   *    *kursuid* => [
   *      0 => 'Kinder gewinnen...'
   *      1 => '',
   *      2 => '39259',
   *    ],
   *    *kursuid* => [
   *      0 => 'Kinder gewinnen...'
   *      1 => '',
   *      2 => '39359',
   *    ],
   *  ]
   * ]
   *
   * An PHP-Excell wird 'keys' als headerarray (ergaenzt um titel und kursleiter) uebergeben,
   * 'values' wird als vollstaendiges Array fuer alle Spalten und Reihen des Excels uebergeben mit Kurswerten
   *
   * Fuer alle Felder die ein Kurs in der Statistik nicht hat ("customfeld 1" fuer Kurs blabla) wird trotzdem ein Array Eintrag gemacht,
   * welcher aber einfach ein leerer String ist. Jede Reihe muss die gleiche anzahl Spalten haben.
   *
   * * TODO Schedulertask erstellen, der alte Statistiken periodisch loescht
   */
  public function downloadStatsAction() {

    // Berechtigungen Check
    $this->initFeuser();
    if (!$this->feuser_uid) {
      $this->addFlashMessage($this->translateSystemMessages('sys_zugriff_verweigert_kurse_hochladen'), $this->translateSystemMessages('sys_zugriff_verweigert'), AbstractMessage::ERROR);
      $this->redirect('list');
    }

    // Wenn auf Download geklickt wird, file generieren.
    $args = $this->request->getArguments();
    $download = $args['download'];

    if($download) {
      // Kurse mit ausgefüllter Statistik auslesen
      $courses = array();
      $this->getAndAssignCourses($courses, true, true);

      if (count($courses) == 0) {
        $this->addFlashMessage("Es wurden keine Statistik-Datensätze für den Download gefunden", "Keine Einträge", AbstractMessage::ERROR);
        return;
      }

      // Zuerst ueber alle Kurse loopen, damit wir alle Statistik-Felder zur verfuegung haben
      $excel_array = array();
      foreach ($courses as $uid => $course) {
        $statistik_array = json_decode($course['statistik']);
        foreach($statistik_array as $key => $value) {
          // Die Statistik-Felder in key-array speichern fuer spaetere Verwendung.
          $excel_array['keys'][$key] = $key;
        }
      }

      //Sortierung:
      /* 1. Dimension = Rows
       * 2. Dimension = Spalten
       *
       * Reihenfolge der Spalten (in den meisten faellen):
       * titel (immer),kursleiter (immer),interne_id (immer),durchgefhrt,anzahlteilnehmerweiblich,anzahlteilnehmermnnlich,anzahlteilnehmerkinder,
       * customfelder
       */
      foreach ($courses as $uid => $course) {
        // Kategorien auslesen und dem Template zuweisen
        $categs = array();
        foreach ($this->coursesRepository->findCategories($this->settings['categoryFilterRootnode']) as $record_uid => $record) {
          $subcategories = $this->coursesRepository->findCourseCategories($uid, $record_uid);
          $categs[] = array(
            'uid' => $record_uid,
            'title' => $record['title'],
            'elements' => $subcategories
          );
        }

        //Statistik-Feld von Kurs auslesen
        $statistik_array = json_decode($course['statistik']);
        //Mit abfuellen der Excel Felder beginnen (als erstes Titel und Kursleiter)
        $excel_array['values'][$uid][] = ($course['titel']) ? $course['titel'] : '';
        $excel_array['values'][$uid][] = ($course['start_datum']) ? date('d.m.Y',$course['start_datum']) : '';
        $excel_array['values'][$uid][] = ($course['end_datum']) ? date('d.m.Y',$course['end_datum']) : '';
        $excel_array['values'][$uid][] = ($course['kursleiter']) ?: $course['teacher_text'];

        // Immer 5 Parentkategorien hier. Wenn nicht, dann muss Code angepasst werden
        // Kurs-Segment, Zielgruppen, Kanton / Bezirk, Kurssprache, Fokus

        if(!empty($categs)) {
          $course['categs'] = $categs;
          foreach ($categs as $parent_category) {
            $category_field = '';
            if(!empty($parent_category['elements'])) {
              foreach($parent_category['elements'] as $subcategory) {
                $category_field .= $subcategory['title'].';';
              }
              $category_field = substr($category_field,0,-1);
            }
            $excel_array['values'][$uid][] = $category_field;
          }
        }

        // Loop ueber key-array um die richtige Spalten abzufuellen (leer wo nicht vorhanden)
        foreach($excel_array['keys'] as $excel_key => $excel_value) {
          // Fuer jeden Key zuerst davon ausgehen, dass es ihn nicht gibt fuer diesen Kurs
          $keyvalue = '';
          // Ueber Statistik-Felder des Kurses Loopen
          foreach($statistik_array as $statistik_key => $statistik_value) {
            // Wenn Key in Statistik-Feldern existiert, dann abfuellen!
            if($excel_key == $statistik_key) {
              $keyvalue = $statistik_value;
            }
          }
          // Wert setzen
          $excel_array['values'][$uid][] = $keyvalue;
        }

        $category_arrays = array();
        foreach($categs as $category_parent) {
          $category_arrays[] = $category_parent['title'];
        }
        // Reihenfolge fuer Header umkehren
        $category_arrays = array_reverse($category_arrays);
      }

      // Header Array muss bevor es uebergeben wird um Titel und Kursleiter ergaenzt werden, da nicht in Statistik-Feldern vorhanden
      $header_array = ($excel_array['keys']) ?: array();

      // Parent-Kategorien dem Header hinzufuegen
      foreach($category_arrays as $category) {
        // In For-Schleife vorher umgekehrt, damit richtige Reihenfolge
        array_unshift($header_array,$category);
      }

      // Kurs-Standard Daten in Header
      array_unshift($header_array,'Titel','Startdatum','Enddatum', 'Kursleiter');

      // Generierung Excel via InsorLibrary
      $path = getcwd().'/typo3temp/is_helper/statistik_downloads';
      if (!file_exists($path)) {
        mkdir($path, 0777, true);
      }
      if (count($excel_array['values'])) {
        $path = InsorLib::writeExcel($excel_array['values'], $header_array,'statistik_downloads/statistik',$this->feuser_uid);
        // Download Link an Frontend uebergeben.
        $this->view->assign('download_link',$path);
      } else {
        $this->view->assign('download_link','');
      }

      $this->view->assign('download',1);

      // Statistik-Count
      $this->view->assign('statistik_count',count($excel_array['values']));
      header("Location: $path");
    }
  }

  /**
   * Uebersetzung via locallang
   *
   * @param $sys_string
   * @return NULL|string
   */
  public function translateSystemMessages($sys_string) {
    return \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_iscourses2.'.$sys_string, 'is_courses2');
  }

  /**
  * Zeigt den aktuellen Kurs an - in der Regel auf der Page oberhalb eines Anmeldeformulares
  * 
  * @param integer $course_uid
  * @return void
  */
  public function showAction($course_uid = 0)
  {
    // Falls Kurs Parameter nicht angegeben, Meldung ausgeben und beenden
    if ($course_uid == 0) {
      $this->addFlashMessage($this->translateSystemMessages('sys_fehlende_kurs_uid'), $this->translateSystemMessages('sys_fehler'), AbstractMessage::ERROR);
      return;
    }

    // Kurs auslesen
    $course = $this->coursesRepository->findByUid($course_uid);

    if ($course['hidden']) {
      $this->addFlashMessage($this->translateSystemMessages('sys_kurs_nicht_freigegeben'), $this->translateSystemMessages('sys_kurs_nicht_veroeffentlicht'), AbstractMessage::WARNING);
    }

    //if (($course['end_datum'] < date('U')) || ($course['status'] == 3) || ($course['status'] == 4) || ($course_uid == 0)) {
    if (($course['end_datum'] < date('U')) || ($course['status'] == 3) || ($course['status'] == 4) || ($course['anz_teilnehmer'] >= $course['max_teilnehmer'])) {
      //print_r($this->settings);
      //exit;
      $redirectPid = $this->settings['errorPagePid'];

      if (($redirectPid) && ($redirectPid != $GLOBALS['TSFE']->id)) {
        $this->addFlashMessage($this->translateSystemMessages('sys_kurs_anmeldung_nicht_moeglich') . ": {$course['titel']}", $this->translateSystemMessages('sys_fehler'), AbstractMessage::ERROR);
        $uriBuilder = $this->uriBuilder;
        $uri = $uriBuilder
          ->setTargetPageUid($redirectPid)
          ->setArguments(array(
            'tx_iscourses2_courses' => array(
              'course_uid' => $course_uid,
              'action' => 'show',
              'controller' => 'Courses'
            )
          ))
          ->build();
        $this->redirectToUri($uri, 0, 404);
      }
    }

    // Check, ob Kurs vom FE User beschrieben werden kann
    $this->initFeuser();
    if ($this->hasUserCourseWritePermission($course_uid)) {
      $course['writable'] = true;
    }

    // Falls Kurs leer, dann eine Meldung ausgeben
    if (!$course) {
      $this->addFlashMessage($this->translateSystemMessages('sys_kurs_existiert_nicht_mehr'), $this->translateSystemMessages('sys_kurs_nicht_gefunden'), AbstractMessage::WARNING);
    }

    // Page Title setzen
    //$GLOBALS['TSFE']->page['title'] = "{$course['titel']}, {$course['start_datum_formatted']}";
    $GLOBALS['TSFE']->page['title'] = "{$course['titel']}, " . substr($course['start_datum_formatted'], 0, 10);

    // Kurs-Unterlagen decodieren
    $course['unterlagen_links'] = json_decode($course['unterlagen_links'], true);

    // Falls Anmelde_Url ein String und gesetzt ist, so haben wir einen Link auf ein externes Angebot
    if ($course['anmelde_url'] && !intval($course['anmelde_url'])) {
      $course['external_signupurl'] = $course['anmelde_url'];
    }

    // Anmelde-URL Verarbeitung
    if ($course['anmelde_url']) {
      $course['anmelde_url'] = $this->sanitizeUrl($course['anmelde_url']);
    }
    if ($course['detail_url']) {
      $course['detail_url'] = $this->sanitizeUrl($course['detail_url']);
    }

    // Bild zum Kurs
    if ($course['picture']) {
      $course['picture'] = $this->coursesRepository->getPicturesOfCourse($course_uid);
    }

    // URL bauen, wird für Rich Snippet verwendet
    foreach ($this->settings['urlParameterMapping'] as $field => $param) {
      if ($param) {
        $params[$param] = $course[$field];
      }
    }
    $course['params'] = $params;

    // Anmeldung noch möglich?
    $course['anmeldung_moeglich'] = ($course['end_datum'] > date('U'));

    // Einige weitere Infos für das Rich Snippet
    $course['kosten_int'] = intval(filter_var($course['kosten'], FILTER_SANITIZE_NUMBER_INT));

    // Zuweisung ins Template
    $this->view->assign('course', $course);

    // Lehrer dem Template zuweisen
    $teacher = $this->coursesRepository->findCourseTeachers($course_uid);
    $this->view->assign('teachers', $teacher);

    // Kategorien auslesen und dem Template zuweisen
    foreach ($this->coursesRepository->findCategories($this->settings['categoryFilterRootnode']) as $uid => $record) {
      $subcategories = $this->coursesRepository->findCourseCategories($course_uid, $uid);
      $categs[] = array(
        'uid' => $uid,
        'title' => $record['title'],
        'elements' => $subcategories
      );
    }

    // Die Kategorien dem Template zuweisen
    $this->view->assign('categs', $categs);

    // Das Kursbild
    $coursepic = $this->getCoursePicturePath($course_uid, array_values($teacher)[0]['uid']);
    $this->view->assign('coursepic', $coursepic);

    // Opengraph Tags
    if (strpos($coursepic, 'logo.dummy.jpg') === false) {
      $GLOBALS['TSFE']->additionalHeaderData['og:image'] = "<meta property='og:image' content='{$_SERVER['REQUEST_SCHEME']}://{$_SERVER['SERVER_NAME']}/$coursepic' >";
    }

    // Parameter Mapping für Powermail Hidden Felder
    $params = array();
    foreach ($this->settings['powermailParameterMapping'] as $field => $param) {
      if ($param) {
        $course[$field] = explode('<br />', $course[$field]);
        $course[$field] = array_filter($course[$field], function($val) {
          return (trim($val)) ? true : false;
        });

        $course[$field] = implode(', ', $course[$field]);
        $params[$param] = $course[$field];
      }
    }
    $this->view->assign('powermailParameterMapping', json_encode($params));

  }

  /**
   * Zeigt Maske für die Statistik-Eingabe an
   *
   * @param int $course_uid
   * @param string $hmac
   */
  public function statsAction($course_uid = 0, $hmac = '') {

    if (isset($_GET['tx_iscourses2_']['course_uid'])) {
      $course_uid = intval($_GET['tx_iscourses2_']['course_uid']);
    }

    if (isset($_GET['tx_iscourses2_']['hmac'])) {
      $hmac = $_GET['tx_iscourses2_']['hmac'];
    }

    // Check, ob der (Nicht-FE-)User die Statistik eingeben darf
    if ($hmac != sha1("$course_uid-{$this->secret}")) {
      $this->addFlashMessage($this->translateSystemMessages('sys_kurs_falscher_stats_link') . " / DEBUG: valid hmac would be " . sha1("$course_uid-{$this->secret}"), $this->translateSystemMessages('sys_fehler'), AbstractMessage::ERROR);
      return;
    }

    // Falls Kurs Parameter nicht angegeben, Meldung ausgeben und beenden
    if ($course_uid == 0) {
      $this->addFlashMessage($this->translateSystemMessages('sys_fehlende_kurs_uid'), $this->translateSystemMessages('sys_fehler'), AbstractMessage::ERROR);
      return;
    }

    // Kurs auslesen
    $course = $this->coursesRepository->findByUid($course_uid);

    // Falls Kurs leer, dann eine Meldung ausgeben
    if (!$course) {
      $this->addFlashMessage($this->translateSystemMessages('sys_kurs_existiert_nicht_mehr'), $this->translateSystemMessages('sys_kurs_nicht_gefunden'), AbstractMessage::WARNING);
      return;
    }

    // Falls schon statistische Daten existieren, eine Meldung ausgeben
//    if ($course['statistik']) {
//      $this->addFlashMessage($this->translateSystemMessages('sys_kurs_stats_schon_eingereicht'), $this->translateSystemMessages('sys_fehler'), AbstractMessage::ERROR);
//      return;
//    }

    // Falls der Powermail Parameter fehlt, hängen wir diesen an, damit das Formular weiss, für welchen Kurs
    // diese Statistik ist!
    if (!$_GET['tx_powermail_pi1']['field']['interne_id']) {
      $uri = "{$_SERVER['REQUEST_SCHEME']}://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
      header("Location: $uri&tx_powermail_pi1[field][interne_id]=$course_uid");
    }

    // Lehrer dem Template zuweisen
    $teacher = $this->coursesRepository->findCourseTeachers($course_uid);
    $this->view->assign('teachers', $teacher);

    // Zuweisung ins Template
    $this->view->assign('course', $course);

  }

  protected function sanitizeUrl($url) {
    if (!intval($url)) {
      if ((strpos($url, '@') !== false) && (strpos($url, 'mailto:') === false)) {
        $url = "mailto:$url";
      }

      else if (strpos($url, 'http') === false) {
        $url = "http://$url";
      }
    }

    return $url;
  }

  /**
   * Zeigt Maske zum editieren des Kurs-Objektes an
   *
   * @param int $course_uid
   */
  public function editAction($course_uid = 0) {
    // Berechtigungen Check
    $this->initFeuser();
    if (!$this->feuser_uid) {
      $this->addFlashMessage($this->translateSystemMessages('sys_zugriff_verweigert_kurse_erstellen'), $this->translateSystemMessages('sys_zugriff_verweigert'), AbstractMessage::ERROR);
      $this->redirect('list');
    }

    if ($course_uid) {
      if (!$this->hasUserCourseWritePermission($course_uid)) {
        $this->addFlashMessage($this->translateSystemMessages('sys_zugriff_verweigert_kurse_bearbeiten'), $this->translateSystemMessages('sys_zugriff_verweigert'), AbstractMessage::ERROR);
        $this->redirect('list');
      }

      // Kurs auslesen und dem Template zuweisen
      $course = $this->coursesRepository->findByUid($course_uid);

      // Soll der Kurs kopiert werden? Falls ja, UID wieder auf 0 setzen
      $args = $this->request->getArguments();
      if ($args['copyToNew']) {
        $course['uid'] = 0;
        $this->view->assign('copyToNew', 1);
      }

      // Daten für Link auf das Statistik-Eingabge-Formular
      $statsFormData = $this->getStatsFormLink($course);
      $course['statsFormHmac'] = $statsFormData['statsFormHmac'];
      $course['statsParams'] = $statsFormData['statsParams'];
      $course['statsFormPid'] = $statsFormData['statsFormPid'];
      if ($course['end_datum'] < date('U')) {
        $course['statsOverdue'] = true;
      }

      $this->view->assign('course', $course);

      // Falls Kurs leer, dann eine Meldung ausgeben
      if (!$course) {
        $this->addFlashMessage($this->translateSystemMessages('sys_kurs_existiert_nicht_mehr'), $this->translateSystemMessages('sys_kurs_nicht_gefunden'), AbstractMessage::WARNING);
        $this->redirect('list');
      }

      // Dem Kurs zugeordnete Kategorien auslesen
      $my_categories = array_keys($this->coursesRepository->findCourseCategories($course_uid));

    }

    // Mögliche Kategorien auslesen und vorselektieren
    foreach ($this->coursesRepository->findCategories($this->settings['categoryFilterRootnode']) as $uid => $record) {

      $subcategories = $this->coursesRepository->findCategories($uid);
      foreach ($subcategories as &$cat) {
        if (in_array($cat['uid'], $my_categories)) {
          $cat['selected'] = 'selected';
        }
      }
      $categs[] = array(
        'uid' => $uid,
        'title' => $record['title'],
        'elements' => $subcategories
      );
    }

    // Die Kategorien dem Template zuweisen
    $this->view->assign('categs', $categs);

    // Mögliche Lehrer auslesen und dem Template zuweisen
    $teachers = $this->teachersRepository->findAll();

    // Dem Kurs zugeordnete FE-User auslesen und vorselektieren
    if ($course_uid) {
      foreach ($this->coursesRepository->findCourseTeachers($course_uid) as $uid => $record) {
        if($teachers[$uid]) {
          $teachers[$uid]['selected'] = 'selected';
          $one_teacher = $uid;
        }
      }
    }
    $this->view->assign('teachers', $teachers);

    // Das Kursbild
    $this->view->assign('coursepic', $this->getCoursePicturePath($course_uid, $one_teacher));

  }

  /**
   * Aktualisiert das Kurs-Objekt
   * @param int $redirect_after
   */
  public function updateAction($redirect_after = null) {
    $args = $this->request->getArguments();
    $course_uid = $args['course_uid'];

    // Berechtigungen Check
    $this->initFeuser();
    if (($course_uid) && (!$this->hasUserCourseWritePermission($course_uid))) {
      $this->addFlashMessage($this->translateSystemMessages('sys_zugriff_verweigert_kurse_bearbeiten'), $this->translateSystemMessages('sys_zugriff_verweigert'), AbstractMessage::ERROR);
      return;
    }

    // Handelt es sich um einen neuen Kurs?
    $newCourse = ($course_uid) ? false : true;

    // Bei einem neuen Kurs, den Owner, die PID und das Hidden Flag einmalig setzen
    if ($newCourse) {
      $args['course']['owner'] = $this->feuser_uid;
      $args['course']['pid'] = $this->settings['storagePid'];
      $args['course']['hidden'] = 1;
    }

    // Herausfinden, ob wir Duplikate haben
    if ($newCourse) {
      $similars = $this->coursesRepository->findSimilar($args['course'], 50);
      if (count($similars)) {
        $similar = array_values($similars)[0];
        $start_datum_similar = date("d.m.Y", $similar['start_datum']);
        $this->addFlashMessage("Bestehende Veranstaltung '{$similar['titel']}' vom $start_datum_similar gefunden, {$similar['similarity_score']}% Übereinstimmung", 'Duplikate-Warnung, Datensatz nicht angelegt', AbstractMessage::ERROR);
        $this->redirect('edit', null, null, null);
      }
    }

    // Felder und Kategorien abspeichern
    $course_uid = $this->coursesRepository->updateCourse($course_uid, $args['course']);
    $this->coursesRepository->setNewCourseCategories($course_uid, $args['categories']);

    // Gewählte FE-User diesem Kurs hinterlegen, falls es ein neuer Kurs ist
    $this->coursesRepository->replaceFeusersToCourse($course_uid, $args['teachers']);

    // Kursbild abspeichern
    if ($args['coursepic']['tmp_name'] && $args['logo']['error'] == 0) {
      $destination = GeneralUtility::getFileAbsFileName("uploads/tx_iscourses2/coursepics/");
      if(!is_dir($destination)) {
        mkdir($destination, 0755, true);
      }
      move_uploaded_file($args['coursepic']['tmp_name'], $destination . "coursepic.{$course_uid}.jpg");
      $this->coursesRepository->removeFileReference("coursepics/coursepic.$course_uid.jpg");
    }

    // Ist es ein richtiger Kurs? Dann Cache löschen und Flash-Message anfügen für erfolgreiches speichern
    if (!$args['course']['stats_only_record']) {
      $this->clearPageCaches();
      $flashText = ($newCourse) ? $this->translateSystemMessages('sys_publikation_nach_ueberpruefung') : $this->translateSystemMessages('sys_aenderungen_erfolgreich_gespeichert');
    } else {
      $flashText = $this->translateSystemMessages('sys_aenderungen_erfolgreich_gespeichert');
    }

    $this->addFlashMessage($flashText, '', AbstractMessage::OK);

    // Umleiten auf angegebene Page, oder sonst wieder in die edit action mit dem neu angelegten/gespeicherten Kurs
    if ($redirect_after) {
      $uriBuilder = $this->uriBuilder;
      $uri = $uriBuilder->setTargetPageUid($redirect_after)->build();
      $this->redirectToURI($uri, $delay=0, $statusCode=303);
    } else {
      $this->redirect('edit', null, null, array('course_uid' => $course_uid));
    }


  }

  protected function clearPageCaches() {
    $clearPageCaches = explode(',', $this->settings['clearCacheOnUpdate']);
    if (count($clearPageCaches)) {
      $this->cacheService->clearPageCache($clearPageCaches);
    }
  }

  /**
   * Upload einer CSV Datei entgegen nehmen, und die Kurse daraus importieren. Der Preflight-Modus
   * dient der Validierung des Files, dabei werden keine effektiven Imports ausgelöst.
   */
  public function uploadAction() {
    // Berechtigungen Check
    $this->initFeuser();
    if (!$this->feuser_uid) {
      $this->addFlashMessage($this->translateSystemMessages('sys_zugriff_verweigert_kurse_hochladen'), $this->translateSystemMessages('sys_zugriff_verweigert'), AbstractMessage::ERROR);
      $this->redirect('list');
    }

    // POST/GET Argumente uebernehmen und in args speichern
    $args = $this->request->getArguments();

    // Preflight und Filename seperat abspeichern zum besseren Verstaendnis im Code
    // Preflight = 1 -> 1. Schritt im Formular, parse und zeige Preflight
    // Preflight = 2 -> 2. Schritt im Formular, parse und fuehre import anhand ausgewaehlter Kriterien aus
    $preflight = intval($args['preflight']);
    $preflight_filename = $args['filename'];

    $statistik = $args['statistik'];

    // Wenn Upload abgebrochen wird, auf Upload-Defaultseite redirecten
    if($args['abort'] == 1) {
      $this->redirect('upload');
    }

    // Wenn Upload-Element nicht ausgefuellt, oder kein zwischengespeichertes Excel-Vorhanden, breche ab.
    if ((!$args['importfile']['tmp_name']) && ($preflight != 2 && $preflight_filename == '')) {
      return;
    }

    // Importdatei engegennehmen und auslesen
    if ($args['importfile']['tmp_name'] && $args['importfile']['error'] == 0) {
      // Importfile im Uploads-Verzeichnis tx_iscourses/importfiles zwischenspeichern zur weiteren Verwendung
      $destination = GeneralUtility::getFileAbsFileName("uploads/tx_iscourses2/importfiles/");
      // Falls Verzeichnis noch nicht existiert, erstelle es
      if(!is_dir($destination)) {
        mkdir($destination, 0755, true);
      }
      // Datei als feuser-timestamp.csv abspeichern
      $filename = $destination . $this->feuser_uid. "-" . time() . ".csv";
      move_uploaded_file($args['importfile']['tmp_name'], $filename);
      // Importfile parsen
      $excelContent = InsorLib::FileGetExcel($filename);
    }

    // Parse bestehendes Excel-File fuer Import nach Preflight
    if($preflight == 2 && $preflight_filename != '') {
      $excelContent = InsorLib::FileGetExcel($preflight_filename);
    }

    // Importdatei Inhaltscheck
    if (!$excelContent) {
      $this->addFlashMessage($this->translateSystemMessages('sys_fehler_importdatei_fehlerhaft'), '', AbstractMessage::ERROR);
      $error = true;
    }

    // CSV Header Definition
    foreach($this->settings['courseImportMappings']['fields'] as $dbfield => $csvfield) {
      if (is_array($csvfield)) $csvfield = $csvfield['0'];
      $soll_header[] = $csvfield;
    }

    // CSV Header Check
    foreach($soll_header as $column) {
      if (!array_key_exists($column, $excelContent[0])) {
        $this->addFlashMessage($this->translateSystemMessages('sys_fehler_importdatei_fehlende_spalte') .$column. $this->translateSystemMessages('sys_fehler_importdatei_fehlende_spalte_in_Importdatei_nicht_vorhanden'), '', AbstractMessage::ERROR);
        $error = true;
      }
    }

    // Erste N Zeilen überspringen (enthalten keine Import Daten)
    for ($i = 0; $i < $this->settings['courseImportMappings']['nofHeaderlinesToSkip'] - 1; $i++) {
      array_shift($excelContent);
    }

    // Fehler gefunden? Dann Action verlassen.
    if ($error) {
      $this->redirect('upload');
    }

    // Nun die Records in die DB schreiben
    // Counter wird fuer Zeile benoetigt, da Form-Field dynamisch generiert wird anhand Zeilennummer
    $excelContent_counter = 1;
    foreach($excelContent as $line) {

      // Erstes Feld muss bestückt sein.
      // TODO: Muss-Felder per TS konfigurieren
      if (!array_values($line)[0]) continue;

      // Vorbereitung Werte-Array fuer Zeile
      $values = array();
      $date_value_fields = array();

      // Import Felder abarbeiten
      foreach($this->settings['courseImportMappings']['fields'] as $key => $field) {
        // Wenn Feld ein Array ist, gehe hinein, andernfalls direkt in value-array schreiben
        if (is_array($field)) {
          // Typoscript Key als Key benutzen
          $values[$key] = $line[$field['_typoScriptNodeValue']];

          // Boolean Format
          if (isset($field['trueValue'])) {
            $values[$key] = ($values[$key] == $field['trueValue']) ? 1 : 0;
          }

          // Datumsformat
          if (isset($field['isDate'])) {
            $date_value_fields[] = $key;
            // Excel Datumsformat Integer
            if (is_numeric($values[$key])) {
              $timestamp = ($values[$key] - 25569) * 86400;
              $values[$key] = date('d.m.Y', $timestamp);
            }
          }

          // JSON Datensätze
          if (isset($field['json'])) {
            foreach($field['json'] as $jsonkey => $excelfield) {
              $json[$jsonkey] = $line[$excelfield];
            }
            $values[$key] = json_encode($json);
          }

          // Statische Fixwerte
          if (isset($field['value'])) {
            $values[$key] = $field['value'];
          }
        } else {
          $values[$key] = $line[$field];
        }
      }

      // Log Eintrag für Import schreiben
      $values['log'] = date("Y-m-d H:i:s") . " Import über Web-GUI";

      // Kategorien abarbeiten, durch Semikolon getrennt
      $categories_to_set = array();
      // Mappings aus Typoscript lesen
      foreach($this->settings['courseImportMappings']['categories']['column'] as $field) {
        foreach(explode(';', $line[$field['caption']]) as $external_id) {
          $categories_to_set[] = $field['mappings'][trim($external_id)];
        }
      }

      // Fixierte Kategorien - also solche, die immer gesetzt werden sollen
      foreach(explode(',', $this->settings['courseImportMappings']['categories']['setFixed']) as $field) {
        $categories_to_set[] = intval($field);
      }

      // Leere Einträge entfernen
      $categories_to_set = array_filter($categories_to_set);

      // Owner setzen
      $values['owner'] = $this->feuser_uid;

      // Storage PID setzen
      $values['pid'] = $this->settings['storagePid'];

      // Kurs auf Hidden setzen
      $values['hidden'] = 1;

      // Versuchen, den Lehrer zu finden
      if(is_numeric($values['teacher_text'])) {
        $fe_user = $this->teachersRepository->findByUid($values['teacher_text'], true);
      } else {
        $fe_user = ($values['teacher_text']) ? $this->teachersRepository->findByName($values['teacher_text'],true) : 0;

        if($fe_user == null) {
          $fe_user = $this->teachersRepository->findByUsername($values['teacher_text'],true);
          if($fe_user == null) {
            $fe_user = 0;
          }
        }
      }


      // Preflight: Slashes entfernen
      foreach ($values as &$el) {
        if(!is_array($el) && !is_int($el)) {
          $el = stripslashes($el);
        }
      }

      // Wenn Schritt 1 (Preflight)
      if ($preflight == 1) {
        // Kategorien kommagetrennt darstellen
        $values['categories'] = implode(', ', $categories_to_set);

        // Lehrer aus DB setzen falls gegeben
        if ($fe_user && $fe_user != 0) {
          $values['teacher'] = "{$fe_user['username']} (ID:{$fe_user['uid']})";
        } else {
          $values['teacher'] = 0;
        }

        // Similars setzen
        $values = $this->findAndSetPreflightSimilars($values,$date_value_fields);

        // Folgende Felder unsetten, da diese nicht im Preflight dargestellt werden duerfen
        unset($values['owner']);
        unset($values['pid']);
        unset($values['hidden']);
        unset($values['stats_only_record']);
        unset($values['log']);

        if($statistik == 1) {
          unset($values['teacher']);
          foreach($values['similars']['items'] as $similar_key => $similar) {
            unset($values['similars']['items'][$similar_key]['teacher']);
          }
        }

        array_unshift($values, ++$valuecounter);
        $preflight_values[] = $values;

        }
      // Wenn Schritt 2, Upload nach Preflight
      else if ($preflight == 2) {
        // Similars entfernen, da diese sonst im SQL fehler werfen wuerden
        unset($values['similars']);

        if($statistik == 1) {
          unset($values['teacher']);
        }

        // Formular-Wert der Zeilennummer auslesen
        // Wenn = new, dann Kurs neu erstellen
        if($args['select-action-'.$excelContent_counter] == 'new') {
          // Kursupload via kursUpload-Funktion
          $course_uid = $this->kursUpload($values,$categories_to_set,$fe_user);

          // Konnte Kurs erfolgreich erstellt werden?
          if($course_uid) {
            $new_uids[] = $course_uid;
          }
        }

        // Wenn = ignore, dann ignoriere zu importierenden Kurs
        else if ($args['select-action-'.$excelContent_counter] == 'ignore') {}

        // In anderem Fall wird der ausgewaehlte Kurs ueberschrieben
        else {
          $course_uid = $args['select-action-'.$excelContent_counter];
          $course_uid = $this->kursUpload($values,$categories_to_set,$fe_user,$course_uid,1);
          $updated_uids[] = $course_uid;
        }
      }
      // Zeilennummer hochzaehlen
      $excelContent_counter ++;
    }

    // Wenn Schritt 1: Preflight
    if ($preflight == 1) {
      $this->addFlashMessage($this->translateSystemMessages('sys_import_vorab_check'), '', AbstractMessage::INFO);

      // Werte an Frontend uebergeben
      $this->view->assign('preflight_values', $preflight_values);
      // Fieldcount wird fuer colspan benoetigt
      $this->view->assign('preflight_values_fieldcount', count($preflight_values[0])-1);
      $this->view->assign('filename', $filename);

      // Preflight-Template setzen
      $this->view->setTemplatePathAndFilename(ExtensionManagementUtility::extPath('is_courses2') . 'Resources/Private/Templates/Courses/Upload-Preflight.html');
    }

    // Wenn Schritt 2: Upload
    else if($preflight == 2) {
      // Upload-Template setzen
      $this->view->setTemplatePathAndFilename(ExtensionManagementUtility::extPath('is_courses2') . 'Resources/Private/Templates/Courses/UploadFinished.html');
    }

    // Wenn anderes, dann redirect auf Listenansicht (sollte nie vorkommen)
    else {
      // Caches löschen
      $this->clearPageCaches();
      $this->addFlashMessage(count($new_uids) . $this->translateSystemMessages('sys_kurse_erfolgreich_importiert'), '', AbstractMessage::OK);
      $new_uids = implode(',', $new_uids);
      $this->redirect('list', null, null, array('course_uids' => $new_uids));
    }

  }

  /**
   * @param $values
   * @param $date_value_fields
   * @return mixed
   */
  private function findAndSetPreflightSimilars($values,$date_value_fields) {
    // Vorbelegung von Array mit Standard-Werten
    $value_model = $values;

    // Überprüfen, ob wir Duplikate haben
    $similars = $this->coursesRepository->findSimilar($values, 50);
    if (count($similars)) {
      // Warnung abspeichern und Array fuer Similars vorbereiten
      $warnung_text = LocalizationUtility::translate("tx_iscourses2.kurs_datei_upload_duplikate_text", "is_courses2");
      $values['similars']['warnung'] = "<span style='color: red'>".$warnung_text."</span>";
      $values['similars']['items'] = array();

      // Ueber gefundene Resultate loopen
      foreach($similars as $item) {
        // Ueber Felder des vorbereiteten Arrays loopen
        foreach($value_model as $value_model_key => $value_model_field) {
          // Wert vom Similar ins vorbereitete Feld des Arrays abfuellen
          $value = $item[$value_model_key];
          // Bei Date-Feldern noch das Datum beachten
          foreach($date_value_fields as $date_key) {
            if($value_model_key == $date_key) {
              $value = date('d.m.Y', $value);
            }
          }
          // Wert definitiv ins Model speichern
          $value_model[$value_model_key] = $value;
        }

        // Metadaten des Similar-Datensatzes in seperates Feld speichern
        $value_model['meta']['uid'] = $item['uid'];
        $value_model['meta']['similarity_score'] = $item['similarity_score'];
        $value_model['meta']['similar_fields'] = $item['similar_fields'];
        $value_model['meta']['same_owner'] = $item['same_owner'];
        $value_model['meta']['owner'] = $item['owner'];

        // Ausgewaehlte Kategorien des Similars finden
        $categories_to_set = $this->coursesRepository->findCourseCategories($item['uid']);
        $category_string = '';
        foreach($categories_to_set as $category) {
          $category_string .= $category['uid'].',';
        }
        $value_model['categories'] = $category_string;

        // Similar dem Hauptdatensatz hinzufuegen
        $values['similars']['items'][$item['uid']] = $value_model;

        // Model erneut wieder vorbereiten
        $value_model = $values;
      }

      // Standard-Option "Als neuen Kurs erstellen" fuer jeden Eintrag bereitstellen
      $neuerkurs_text = LocalizationUtility::translate("tx_iscourses2.kurs_datei_upload_neuer_kurs","is_courses2");
      $values['similars']['similars_options']['new'] = $neuerkurs_text;

      // Pro gefundenem Similar eine Formular-Option
      foreach($values['similars']['items'] as $similar_key => $similar) {
        // Kurs darf natuerlich nur ueberschrieben werden duerfen, wenn der Owner derselbe ist
        if($similar['meta']['same_owner'] == 1) {
          $overwrite_text = LocalizationUtility::translate("tx_iscourses2.kurs_datei_upload_overwrite","is_courses2");
          $values['similars']['similars_options'][$similar['meta']['uid']] = 'ID '.$similar['meta']['uid']." ".$overwrite_text;
        }
        // Maximaler Score schreiben, relevant fuer Farbgebung und Warnungsmeldung
        if($similar['meta']['similarity_score'] > $values['similars']['similars_maxfactor']) {
          $values['similars']['similars_maxfactor'] = $similar['meta']['similarity_score'];
        }

        // Nicht benoetigte Werte fuer Frontend entfernen
        unset($values['similars']['items'][$similar_key]['owner']);
        unset($values['similars']['items'][$similar_key]['pid']);
        unset($values['similars']['items'][$similar_key]['hidden']);
        unset($values['similars']['items'][$similar_key]['stats_only_record']);
        unset($values['similars']['items'][$similar_key]['log']);
      }

      // Wenn Maxfaktor 100 ist, als Duplikat markieren
      if($values['similars']['similars_maxfactor'] == 100) {
        $values['similars']['status'] = 'duplikat';
      }
      // Andernfalls nur als "Moegliches Duplikat" markieren (Warnung)
      else {
        $values['similars']['status'] = 'warnung';
      }
      // Jedem Eintrag eine Option zu ignorieren hinzufuegen
      $ignore_text = LocalizationUtility::translate("tx_iscourses2.kurs_datei_upload_ignore","is_courses2");
      $values['similars']['similars_options']['ignore'] = $ignore_text;
    }

    // Wenn keine Gueltige Similars, Haupteintrag als OK markieren
    else {

      // Jedem Eintrag eine Option neuer Kurs hinzufügen
      $neuerkurs_text = LocalizationUtility::translate("tx_iscourses2.kurs_datei_upload_neuer_kurs","is_courses2");
      $values['similars']['similars_options']['new'] = $neuerkurs_text;
      // Jedem Eintrag eine Option zu ignorieren hinzufuegen
      $ignore_text = LocalizationUtility::translate("tx_iscourses2.kurs_datei_upload_ignore","is_courses2");
      $values['similars']['similars_options']['ignore'] = $ignore_text;

      $values['similars']['similars_maxfactor'] = 0;
      $values['similars']['status'] = 'ok';
    }

    // Aktualisiertes Array zurueckgeben
    return $values;
  }

  /**
   * Direkter Upload der Daten als neuer Kurs, oder wenn Kurs UID gegeben, update
   *
   * @param $values
   * @param $categories_to_set
   * @param $teacher_fe_user
   * @param int $update_uid
   * @param int $check_owner
   * @return int
   */
  private function kursUpload($values,$categories_to_set,$teacher_fe_user,$update_uid = 0, $check_owner = 0) {
    // Wenn Teacher gegeben, abfuellen
    if ($teacher_fe_user && $teacher_fe_user != 0) {
      $values['teacher'] = $teacher_fe_user['uid'];
    }

    // Wenn Kurs-Uid == 0, dann erstelle einen neuen Kurs
    if($update_uid == 0) {
      $course_uid = $this->coursesRepository->updateCourse(null, $values);
    } else {
      // Zusaetzlicher Check, ob gleicher Owner, ansonsten darf nicht ueberschrieben werden
      if($check_owner && $this->feuser_uid != $values['owner']) {
        // Sollte nie passieren, aber trotzdem hier abgefangen.
      } else {
        // Owner des Kurses ist derselbe, also ok zum ueberschreiben
        $course_uid = $this->coursesRepository->updateCourse($update_uid, $values);
      }
    }

    // Kategorie-Werte muessen immer Integer sein
    foreach($categories_to_set as $key => $field) {
      $categories_to_set[$key] = intval($field);
    }

    // Kategorie-Array vorbereiten, damit in Form [parent => [0] => child] ist
    $categories_to_set = $this->prepareCategoryArray($categories_to_set);

    // Falls Kategorien gesetzt werden müssen, dann dies jetzt noch tun
    if (count($categories_to_set) && intval($course_uid)>0) {
      $this->coursesRepository->setNewCourseCategories($course_uid, $categories_to_set);
    }

    // Lehrer ebenfalls attachen
    if ($teacher_fe_user && $teacher_fe_user != 0 && intval($course_uid)>0) {
      $this->coursesRepository->replaceFeusersToCourse($course_uid, array($teacher_fe_user['uid']));
    }

    // Gib neue/geanderte Kurs-UID zurueck
    if(intval($course_uid)>0) {
      return $course_uid;
    } else {
      return 0;
    }
  }

  private function getCoursePicturePath($course_uid, $teacher_uid) {
    if (is_file(GeneralUtility::getFileAbsFileName("uploads/tx_iscourses2/coursepics/coursepic.$course_uid.jpg"))) {
      return "uploads/tx_iscourses2/coursepics/coursepic.$course_uid.jpg";
    } else if (is_file(GeneralUtility::getFileAbsFileName("uploads/tx_iscourses2/teacherpics/logo.$teacher_uid.jpg"))) {
      return "uploads/tx_iscourses2/teacherpics/logo.$teacher_uid.jpg";
    } else {
      return "uploads/tx_iscourses2/teacherpics/logo.dummy.jpg";
    }
  }

  /**
   * Nimmt ein Array mit Kategorien, sucht die Parents und ordnet in einem neuen Array alle Kategorien einem Parent zu
   *
   * @param array $categories
   * @return array
   */
  private function prepareCategoryArray($categories) {
    $ordered_categories = array();
    foreach($categories as $category) {
      $parent = intval($this->coursesRepository->findParentCategory($category));
      $ordered_categories[$parent][] = $category;
    }
    return $ordered_categories;
  }

  /**
   * Prüft, ob der aktuell eingeloggte User Schreibberechtigung auf den angegebenen Kurs hat
   *
   * @param $course_uid
   * @return bool
   */
  private function hasUserCourseWritePermission($course_uid) {
    return in_array($course_uid, $this->teachercourses ?? array());
  }
}