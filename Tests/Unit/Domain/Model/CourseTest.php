<?php

namespace INSOR\IsCourses2\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \INSOR\IsCourses2\Domain\Model\Course.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class CourseTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
	/**
	 * @var \INSOR\IsCourses2\Domain\Model\Course
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = new \INSOR\IsCourses2\Domain\Model\Course();
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getTitelReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getTitel()
		);
	}

	/**
	 * @test
	 */
	public function setTitelForStringSetsTitel()
	{
		$this->subject->setTitel('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'titel',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getBeschreibungReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getBeschreibung()
		);
	}

	/**
	 * @test
	 */
	public function setBeschreibungForStringSetsBeschreibung()
	{
		$this->subject->setBeschreibung('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'beschreibung',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getKostenReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getKosten()
		);
	}

	/**
	 * @test
	 */
	public function setKostenForStringSetsKosten()
	{
		$this->subject->setKosten('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'kosten',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getDauerReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getDauer()
		);
	}

	/**
	 * @test
	 */
	public function setDauerForStringSetsDauer()
	{
		$this->subject->setDauer('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'dauer',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getKurstageReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getKurstage()
		);
	}

	/**
	 * @test
	 */
	public function setKurstageForStringSetsKurstage()
	{
		$this->subject->setKurstage('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'kurstage',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getDetailUrlTitelReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getDetailUrlTitel()
		);
	}

	/**
	 * @test
	 */
	public function setDetailUrlTitelForStringSetsDetailUrlTitel()
	{
		$this->subject->setDetailUrlTitel('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'detailUrlTitel',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getDetailUrlLinkReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getDetailUrlLink()
		);
	}

	/**
	 * @test
	 */
	public function setDetailUrlLinkForStringSetsDetailUrlLink()
	{
		$this->subject->setDetailUrlLink('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'detailUrlLink',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getAnmeldeUrlReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getAnmeldeUrl()
		);
	}

	/**
	 * @test
	 */
	public function setAnmeldeUrlForStringSetsAnmeldeUrl()
	{
		$this->subject->setAnmeldeUrl('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'anmeldeUrl',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getKursAdresseReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getKursAdresse()
		);
	}

	/**
	 * @test
	 */
	public function setKursAdresseForStringSetsKursAdresse()
	{
		$this->subject->setKursAdresse('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'kursAdresse',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getKursortReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getKursort()
		);
	}

	/**
	 * @test
	 */
	public function setKursortForStringSetsKursort()
	{
		$this->subject->setKursort('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'kursort',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getStartDatumReturnsInitialValueForDateTime()
	{
		$this->assertEquals(
			NULL,
			$this->subject->getStartDatum()
		);
	}

	/**
	 * @test
	 */
	public function setStartDatumForDateTimeSetsStartDatum()
	{
		$dateTimeFixture = new \DateTime();
		$this->subject->setStartDatum($dateTimeFixture);

		$this->assertAttributeEquals(
			$dateTimeFixture,
			'startDatum',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getEndDatumReturnsInitialValueForDateTime()
	{
		$this->assertEquals(
			NULL,
			$this->subject->getEndDatum()
		);
	}

	/**
	 * @test
	 */
	public function setEndDatumForDateTimeSetsEndDatum()
	{
		$dateTimeFixture = new \DateTime();
		$this->subject->setEndDatum($dateTimeFixture);

		$this->assertAttributeEquals(
			$dateTimeFixture,
			'endDatum',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getAnmeldeschlussDatumReturnsInitialValueForDateTime()
	{
		$this->assertEquals(
			NULL,
			$this->subject->getAnmeldeschlussDatum()
		);
	}

	/**
	 * @test
	 */
	public function setAnmeldeschlussDatumForDateTimeSetsAnmeldeschlussDatum()
	{
		$dateTimeFixture = new \DateTime();
		$this->subject->setAnmeldeschlussDatum($dateTimeFixture);

		$this->assertAttributeEquals(
			$dateTimeFixture,
			'anmeldeschlussDatum',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getAnzTeilnehmerReturnsInitialValueForInt()
	{	}

	/**
	 * @test
	 */
	public function setAnzTeilnehmerForIntSetsAnzTeilnehmer()
	{	}

	/**
	 * @test
	 */
	public function getMaxTeilnehmerReturnsInitialValueForInt()
	{	}

	/**
	 * @test
	 */
	public function setMaxTeilnehmerForIntSetsMaxTeilnehmer()
	{	}

	/**
	 * @test
	 */
	public function getStatusReturnsInitialValueForInt()
	{	}

	/**
	 * @test
	 */
	public function setStatusForIntSetsStatus()
	{	}

	/**
	 * @test
	 */
	public function getExternalIdReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getExternalId()
		);
	}

	/**
	 * @test
	 */
	public function setExternalIdForStringSetsExternalId()
	{
		$this->subject->setExternalId('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'externalId',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getStammdatensatzIdReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getStammdatensatzId()
		);
	}

	/**
	 * @test
	 */
	public function setStammdatensatzIdForStringSetsStammdatensatzId()
	{
		$this->subject->setStammdatensatzId('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'stammdatensatzId',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getTeacherReturnsInitialValueForTeachers()
	{
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->subject->getTeacher()
		);
	}

	/**
	 * @test
	 */
	public function setTeacherForObjectStorageContainingTeachersSetsTeacher()
	{
		$teacher = new \INSOR\IsCourses2\Domain\Model\Teachers();
		$objectStorageHoldingExactlyOneTeacher = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$objectStorageHoldingExactlyOneTeacher->attach($teacher);
		$this->subject->setTeacher($objectStorageHoldingExactlyOneTeacher);

		$this->assertAttributeEquals(
			$objectStorageHoldingExactlyOneTeacher,
			'teacher',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function addTeacherToObjectStorageHoldingTeacher()
	{
		$teacher = new \INSOR\IsCourses2\Domain\Model\Teachers();
		$teacherObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('attach'), array(), '', FALSE);
		$teacherObjectStorageMock->expects($this->once())->method('attach')->with($this->equalTo($teacher));
		$this->inject($this->subject, 'teacher', $teacherObjectStorageMock);

		$this->subject->addTeacher($teacher);
	}

	/**
	 * @test
	 */
	public function removeTeacherFromObjectStorageHoldingTeacher()
	{
		$teacher = new \INSOR\IsCourses2\Domain\Model\Teachers();
		$teacherObjectStorageMock = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array('detach'), array(), '', FALSE);
		$teacherObjectStorageMock->expects($this->once())->method('detach')->with($this->equalTo($teacher));
		$this->inject($this->subject, 'teacher', $teacherObjectStorageMock);

		$this->subject->removeTeacher($teacher);

	}
}
