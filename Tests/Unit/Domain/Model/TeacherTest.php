<?php

namespace INSOR\IsCourses2\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \INSOR\IsCourses2\Domain\Model\Teacher.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class TeacherTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
	/**
	 * @var \INSOR\IsCourses2\Domain\Model\Teacher
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = new \INSOR\IsCourses2\Domain\Model\Teacher();
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getExternalIdReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getExternalId()
		);
	}

	/**
	 * @test
	 */
	public function setExternalIdForStringSetsExternalId()
	{
		$this->subject->setExternalId('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'externalId',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getAbschluesseReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getAbschluesse()
		);
	}

	/**
	 * @test
	 */
	public function setAbschluesseForStringSetsAbschluesse()
	{
		$this->subject->setAbschluesse('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'abschluesse',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getThemenReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getThemen()
		);
	}

	/**
	 * @test
	 */
	public function setThemenForStringSetsThemen()
	{
		$this->subject->setThemen('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'themen',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getSchlagworteReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getSchlagworte()
		);
	}

	/**
	 * @test
	 */
	public function setSchlagworteForStringSetsSchlagworte()
	{
		$this->subject->setSchlagworte('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'schlagworte',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getWerdegangReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getWerdegang()
		);
	}

	/**
	 * @test
	 */
	public function setWerdegangForStringSetsWerdegang()
	{
		$this->subject->setWerdegang('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'werdegang',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getIstMitgliedReturnsInitialValueForBool()
	{
		$this->assertSame(
			FALSE,
			$this->subject->getIstMitglied()
		);
	}

	/**
	 * @test
	 */
	public function setIstMitgliedForBoolSetsIstMitglied()
	{
		$this->subject->setIstMitglied(TRUE);

		$this->assertAttributeEquals(
			TRUE,
			'istMitglied',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getEinsatzgebietReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getEinsatzgebiet()
		);
	}

	/**
	 * @test
	 */
	public function setEinsatzgebietForStringSetsEinsatzgebiet()
	{
		$this->subject->setEinsatzgebiet('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'einsatzgebiet',
			$this->subject
		);
	}
}
