<?php
namespace INSOR\IsCourses2\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Courses
 */
class Courses extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * Course name
     *
     * @var string
     * @validate NotEmpty
     */
    protected $titel = '';
    
    /**
     * Short description of the course
     *
     * @var string
     */
    protected $beschreibung = '';
    
    /**
     * How much does attendance cost
     *
     * @var string
     */
    protected $kosten = '';
    
    /**
     * How long will the course dure?
     *
     * @var string
     */
    protected $dauer = '';
    
    /**
     * kurstage
     *
     * @var string
     */
    protected $kurstage = '';
    
    /**
     * detailUrl
     *
     * @var string
     */
    protected $detailUrl = '';
    
    /**
     * Internal or external URL to a signup form
     *
     * @var string
     */
    protected $anmeldeUrl = '';
    
    /**
     * Detailed address of the course location
     *
     * @var string
     */
    protected $kursAdresse = '';
    
    /**
     * Where the course is going to happen
     *
     * @var string
     */
    protected $kursort = '';
    
    /**
     * When will the course start
     *
     * @var \DateTime
     * @validate NotEmpty
     */
    protected $startDatum = null;
    
    /**
     * When will the course end
     *
     * @var \DateTime
     */
    protected $endDatum = null;
    
    /**
     * Until which date will signups be accepted
     *
     * @var \DateTime
     */
    protected $anmeldeschlussDatum = null;
    
    /**
     * How many participants are signed up yet
     *
     * @var int
     */
    protected $anzTeilnehmer = 0;
    
    /**
     * What is the limit of participants
     *
     * @var int
     */
    protected $maxTeilnehmer = 0;
    
    /**
     * States of this course
     *
     * @var int
     */
    protected $status = 0;
    
    /**
     * externalId
     *
     * @var string
     */
    protected $externalId = '';
    
    /**
     * stammdatensatzId
     *
     * @var string
     */
    protected $stammdatensatzId = '';
    
    /**
     * anzeigeBisEnddatum
     *
     * @var bool
     */
    protected $anzeigeBisEnddatum = false;
    
    /**
     * unterlagenLinks
     *
     * @var string
     */
    protected $unterlagenLinks = '';
    
    /**
     * teacherText
     *
     * @var string
     */
    protected $teacherText = '';
    
    /**
     * Persons, which are teaching this course
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\INSOR\IsCourses2\Domain\Model\Teachers>
     */
    protected $teacher = null;
    
    /**
     * Persons, which are teaching this course
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
     */
    protected $owner = null;
    
    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }
    
    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->teacher = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }
    
    /**
     * Returns the titel
     *
     * @return string $titel
     */
    public function getTitel()
    {
        return $this->titel;
    }
    
    /**
     * Sets the titel
     *
     * @param string $titel
     * @return void
     */
    public function setTitel($titel)
    {
        $this->titel = $titel;
    }
    
    /**
     * Returns the beschreibung
     *
     * @return string $beschreibung
     */
    public function getBeschreibung()
    {
        return $this->beschreibung;
    }
    
    /**
     * Sets the beschreibung
     *
     * @param string $beschreibung
     * @return void
     */
    public function setBeschreibung($beschreibung)
    {
        $this->beschreibung = $beschreibung;
    }
    
    /**
     * Returns the kosten
     *
     * @return string $kosten
     */
    public function getKosten()
    {
        return $this->kosten;
    }
    
    /**
     * Sets the kosten
     *
     * @param string $kosten
     * @return void
     */
    public function setKosten($kosten)
    {
        $this->kosten = $kosten;
    }
    
    /**
     * Returns the dauer
     *
     * @return string $dauer
     */
    public function getDauer()
    {
        return $this->dauer;
    }
    
    /**
     * Sets the dauer
     *
     * @param string $dauer
     * @return void
     */
    public function setDauer($dauer)
    {
        $this->dauer = $dauer;
    }
    
    /**
     * Returns the kurstage
     *
     * @return string $kurstage
     */
    public function getKurstage()
    {
        return $this->kurstage;
    }
    
    /**
     * Sets the kurstage
     *
     * @param string $kurstage
     * @return void
     */
    public function setKurstage($kurstage)
    {
        $this->kurstage = $kurstage;
    }
    
    /**
     * Returns the anmeldeUrl
     *
     * @return string $anmeldeUrl
     */
    public function getAnmeldeUrl()
    {
        return $this->anmeldeUrl;
    }
    
    /**
     * Sets the anmeldeUrl
     *
     * @param string $anmeldeUrl
     * @return void
     */
    public function setAnmeldeUrl($anmeldeUrl)
    {
        $this->anmeldeUrl = $anmeldeUrl;
    }
    
    /**
     * Returns the kursAdresse
     *
     * @return string $kursAdresse
     */
    public function getKursAdresse()
    {
        return $this->kursAdresse;
    }
    
    /**
     * Sets the kursAdresse
     *
     * @param string $kursAdresse
     * @return void
     */
    public function setKursAdresse($kursAdresse)
    {
        $this->kursAdresse = $kursAdresse;
    }
    
    /**
     * Returns the kursort
     *
     * @return string $kursort
     */
    public function getKursort()
    {
        return $this->kursort;
    }
    
    /**
     * Sets the kursort
     *
     * @param string $kursort
     * @return void
     */
    public function setKursort($kursort)
    {
        $this->kursort = $kursort;
    }
    
    /**
     * Returns the startDatum
     *
     * @return \DateTime $startDatum
     */
    public function getStartDatum()
    {
        return $this->startDatum;
    }
    
    /**
     * Sets the startDatum
     *
     * @param \DateTime $startDatum
     * @return void
     */
    public function setStartDatum(\DateTime $startDatum)
    {
        $this->startDatum = $startDatum;
    }
    
    /**
     * Returns the endDatum
     *
     * @return \DateTime $endDatum
     */
    public function getEndDatum()
    {
        return $this->endDatum;
    }
    
    /**
     * Sets the endDatum
     *
     * @param \DateTime $endDatum
     * @return void
     */
    public function setEndDatum(\DateTime $endDatum)
    {
        $this->endDatum = $endDatum;
    }
    
    /**
     * Returns the anmeldeschlussDatum
     *
     * @return \DateTime $anmeldeschlussDatum
     */
    public function getAnmeldeschlussDatum()
    {
        return $this->anmeldeschlussDatum;
    }
    
    /**
     * Sets the anmeldeschlussDatum
     *
     * @param \DateTime $anmeldeschlussDatum
     * @return void
     */
    public function setAnmeldeschlussDatum(\DateTime $anmeldeschlussDatum)
    {
        $this->anmeldeschlussDatum = $anmeldeschlussDatum;
    }
    
    /**
     * Returns the anzTeilnehmer
     *
     * @return int $anzTeilnehmer
     */
    public function getAnzTeilnehmer()
    {
        return $this->anzTeilnehmer;
    }
    
    /**
     * Sets the anzTeilnehmer
     *
     * @param int $anzTeilnehmer
     * @return void
     */
    public function setAnzTeilnehmer($anzTeilnehmer)
    {
        $this->anzTeilnehmer = $anzTeilnehmer;
    }
    
    /**
     * Returns the maxTeilnehmer
     *
     * @return int $maxTeilnehmer
     */
    public function getMaxTeilnehmer()
    {
        return $this->maxTeilnehmer;
    }
    
    /**
     * Sets the maxTeilnehmer
     *
     * @param int $maxTeilnehmer
     * @return void
     */
    public function setMaxTeilnehmer($maxTeilnehmer)
    {
        $this->maxTeilnehmer = $maxTeilnehmer;
    }
    
    /**
     * Returns the status
     *
     * @return int $status
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    /**
     * Sets the status
     *
     * @param int $status
     * @return void
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
    
    /**
     * Returns the externalId
     *
     * @return string $externalId
     */
    public function getExternalId()
    {
        return $this->externalId;
    }
    
    /**
     * Sets the externalId
     *
     * @param string $externalId
     * @return void
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }
    
    /**
     * Returns the stammdatensatzId
     *
     * @return string $stammdatensatzId
     */
    public function getStammdatensatzId()
    {
        return $this->stammdatensatzId;
    }
    
    /**
     * Sets the stammdatensatzId
     *
     * @param string $stammdatensatzId
     * @return void
     */
    public function setStammdatensatzId($stammdatensatzId)
    {
        $this->stammdatensatzId = $stammdatensatzId;
    }
    
    /**
     * Adds a Teachers
     *
     * @param \INSOR\IsCourses2\Domain\Model\Teachers $teacher
     * @return void
     */
    public function addTeacher(\INSOR\IsCourses2\Domain\Model\Teachers $teacher)
    {
        $this->teacher->attach($teacher);
    }
    
    /**
     * Removes a Teachers
     *
     * @param \INSOR\IsCourses2\Domain\Model\Teachers $teacherToRemove The Teachers to be removed
     * @return void
     */
    public function removeTeacher(\INSOR\IsCourses2\Domain\Model\Teachers $teacherToRemove)
    {
        $this->teacher->detach($teacherToRemove);
    }
    
    /**
     * Returns the teacher
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\INSOR\IsCourses2\Domain\Model\Teachers> $teacher
     */
    public function getTeacher()
    {
        return $this->teacher;
    }
    
    /**
     * Sets the teacher
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\INSOR\IsCourses2\Domain\Model\Teachers> $teacher
     * @return void
     */
    public function setTeacher(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $teacher)
    {
        $this->teacher = $teacher;
    }
    
    /**
     * Returns the anzeigeBisEnddatum
     *
     * @return bool $anzeigeBisEnddatum
     */
    public function getAnzeigeBisEnddatum()
    {
        return $this->anzeigeBisEnddatum;
    }
    
    /**
     * Sets the anzeigeBisEnddatum
     *
     * @param bool $anzeigeBisEnddatum
     * @return void
     */
    public function setAnzeigeBisEnddatum($anzeigeBisEnddatum)
    {
        $this->anzeigeBisEnddatum = $anzeigeBisEnddatum;
    }
    
    /**
     * Returns the boolean state of anzeigeBisEnddatum
     *
     * @return bool
     */
    public function isAnzeigeBisEnddatum()
    {
        return $this->anzeigeBisEnddatum;
    }
    
    /**
     * Returns the detailUrl
     *
     * @return string detailUrl
     */
    public function getDetailUrl()
    {
        return $this->detailUrl;
    }
    
    /**
     * Sets the detailUrl
     *
     * @param string $detailUrl
     * @return void
     */
    public function setDetailUrl($detailUrl)
    {
        $this->detailUrl = $detailUrl;
    }
    
    /**
     * Returns the unterlagenLinks
     *
     * @return string $unterlagenLinks
     */
    public function getUnterlagenLinks()
    {
        return $this->unterlagenLinks;
    }
    
    /**
     * Sets the unterlagenLinks
     *
     * @param string $unterlagenLinks
     * @return void
     */
    public function setUnterlagenLinks($unterlagenLinks)
    {
        $this->unterlagenLinks = $unterlagenLinks;
    }
    
    /**
     * Returns the teacherText
     *
     * @return string $teacherText
     */
    public function getTeacherText()
    {
        return $this->teacherText;
    }
    
    /**
     * Sets the teacherText
     *
     * @param string $teacherText
     * @return void
     */
    public function setTeacherText($teacherText)
    {
        $this->teacherText = $teacherText;
    }
    
    /**
     * Returns the owner
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FrontendUser $owner
     */
    public function getOwner()
    {
        return $this->owner;
    }
    
    /**
     * Sets the owner
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FrontendUser $owner
     * @return void
     */
    public function setOwner(\TYPO3\CMS\Extbase\Domain\Model\FrontendUser $owner)
    {
        $this->owner = $owner;
    }
    
    /**
     * Returns the picture
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $picture
     */
    public function getPicture()
    {
        return $this->picture;
    }
    
    /**
     * Sets the picture
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $picture
     * @return void
     */
    public function setPicture(\TYPO3\CMS\Extbase\Domain\Model\FileReference $picture)
    {
        $this->picture = $picture;
    }

}
     * picture
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $picture = null;
    
    /**
     * statistik
     *
     * @var string
     */
    protected $statistik = '';
    
    /**
     * log
     *
     * @var string
     */
    protected $log = '';
    
    /**
     * statsOnlyRecord
     *
     * @var bool
     */
    protected $statsOnlyRecord = false;
    
    /**
     * statsReminderSent
     *
     * @var \DateTime
     */
    protected $statsReminderSent = null;
    
    /**
    
    /**
     * Returns the statistik
     *
     * @return string $statistik
     */
    public function getStatistik()
    {
        return $this->statistik;
    }
    
    /**
     * Sets the statistik
     *
     * @param string $statistik
     * @return void
     */
    public function setStatistik($statistik)
    {
        $this->statistik = $statistik;
    }
    
    /**
     * Returns the log
     *
     * @return string $log
     */
    public function getLog()
    {
        return $this->log;
    }
    
    /**
     * Sets the log
     *
     * @param string $log
     * @return void
     */
    public function setLog($log)
    {
        $this->log = $log;
    }
    
    /**
     * Returns the statsOnlyRecord
     *
     * @return bool $statsOnlyRecord
     */
    public function getStatsOnlyRecord()
    {
        return $this->statsOnlyRecord;
    }
    
    /**
     * Sets the statsOnlyRecord
     *
     * @param bool $statsOnlyRecord
     * @return void
     */
    public function setStatsOnlyRecord($statsOnlyRecord)
    {
        $this->statsOnlyRecord = $statsOnlyRecord;
    }
    
    /**
     * Returns the boolean state of statsOnlyRecord
     *
     * @return bool
     */
    public function isStatsOnlyRecord()
    {
        return $this->statsOnlyRecord;
    }
    
    /**
     * Returns the statsReminderSent
     *
     * @return \DateTime $statsReminderSent
     */
    public function getStatsReminderSent()
    {
        return $this->statsReminderSent;
    }
    
    /**
     * Sets the statsReminderSent
     *
     * @param \DateTime $statsReminderSent
     * @return void
     */
    public function setStatsReminderSent(\DateTime $statsReminderSent)
    {
        $this->statsReminderSent = $statsReminderSent;
    }