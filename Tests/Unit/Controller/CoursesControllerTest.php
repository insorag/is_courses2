<?php
namespace INSOR\IsCourses2\Tests\Unit\Controller;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class INSOR\IsCourses2\Controller\CoursesController.
 *
 */
class CoursesControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{

	/**
	 * @var \INSOR\IsCourses2\Controller\CoursesController
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = $this->getMock('INSOR\\IsCourses2\\Controller\\CoursesController', array('redirect', 'forward', 'addFlashMessage'), array(), '', FALSE);
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function listActionFetchesAllCoursessFromRepositoryAndAssignsThemToView()
	{

		$allCoursess = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array(), array(), '', FALSE);

		$coursesRepository = $this->getMock('INSOR\\IsCourses2\\Domain\\Repository\\CoursesRepository', array('findAll'), array(), '', FALSE);
		$coursesRepository->expects($this->once())->method('findAll')->will($this->returnValue($allCoursess));
		$this->inject($this->subject, 'coursesRepository', $coursesRepository);

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$view->expects($this->once())->method('assign')->with('coursess', $allCoursess);
		$this->inject($this->subject, 'view', $view);

		$this->subject->listAction();
	}

	/**
	 * @test
	 */
	public function showActionAssignsTheGivenCoursesToView()
	{
		$courses = new \INSOR\IsCourses2\Domain\Model\Courses();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$this->inject($this->subject, 'view', $view);
		$view->expects($this->once())->method('assign')->with('courses', $courses);

		$this->subject->showAction($courses);
	}

	/**
	 * @test
	 */
	public function editActionAssignsTheGivenCoursesToView()
	{
		$courses = new \INSOR\IsCourses2\Domain\Model\Courses();

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$this->inject($this->subject, 'view', $view);
		$view->expects($this->once())->method('assign')->with('courses', $courses);

		$this->subject->editAction($courses);
	}

	/**
	 * @test
	 */
	public function updateActionUpdatesTheGivenCoursesInCoursesRepository()
	{
		$courses = new \INSOR\IsCourses2\Domain\Model\Courses();

		$coursesRepository = $this->getMock('INSOR\\IsCourses2\\Domain\\Repository\\CoursesRepository', array('update'), array(), '', FALSE);
		$coursesRepository->expects($this->once())->method('update')->with($courses);
		$this->inject($this->subject, 'coursesRepository', $coursesRepository);

		$this->subject->updateAction($courses);
	}

	/**
	 * @test
	 */
	public function deleteActionRemovesTheGivenCoursesFromCoursesRepository()
	{
		$courses = new \INSOR\IsCourses2\Domain\Model\Courses();

		$coursesRepository = $this->getMock('INSOR\\IsCourses2\\Domain\\Repository\\CoursesRepository', array('remove'), array(), '', FALSE);
		$coursesRepository->expects($this->once())->method('remove')->with($courses);
		$this->inject($this->subject, 'coursesRepository', $coursesRepository);

		$this->subject->deleteAction($courses);
	}

	/**
	 * @test
	 */
	public function listActionFetchesAllCoursessFromRepositoryAndAssignsThemToView()
	{

		$allCoursess = $this->getMock('TYPO3\\CMS\\Extbase\\Persistence\\ObjectStorage', array(), array(), '', FALSE);

		$coursesRepository = $this->getMock('INSOR\\IsCourses2\\Domain\\Repository\\CoursesRepository', array('findAll'), array(), '', FALSE);
		$coursesRepository->expects($this->once())->method('findAll')->will($this->returnValue($allCoursess));
		$this->inject($this->subject, 'coursesRepository', $coursesRepository);

		$view = $this->getMock('TYPO3\\CMS\\Extbase\\Mvc\\View\\ViewInterface');
		$view->expects($this->once())->method('assign')->with('coursess', $allCoursess);
		$this->inject($this->subject, 'view', $view);

		$this->subject->listAction();
	}
}
