<?php
namespace INSOR\IsCourses2\Task;

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class StatsMailTask extends \TYPO3\CMS\Scheduler\Task\AbstractTask {

    protected $progress;
    protected $status;

    /**
     * coursesRepository
     *
     * @var \INSOR\IsCourses2\Domain\Repository\CoursesRepository
     * */
  protected $coursesRepository = NULL;

    /**
     * teachersRepository
     *
     * @var \INSOR\IsCourses2\Domain\Repository\TeachersRepository
     * */
  protected $teachersRepository = NULL;

    /**
     * @var $objectManager \TYPO3\CMS\Extbase\Object\ObjectManager
     */
    protected $objectManager;

    /**
     * @var $objectManager \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer
     */
    protected $contentObjectRenderer;

    /**
     * Standard-Statistik-Formular
     * TODO wird eigentlich aus Typoscript ausgelesen, eventuell alternative suchen -> Extensionkonfiguration!?
     *
     * @var integer
     */
    private $defaultStatsFormPid = 234;

    // TODO: Neu den encryptionKey aus LocalConfiguration verwenden
    private $secret = "549af0f6-3117-4ad6-b7e7-0f547d67ffe1";

    /**
     * Statistik Mail Sender
     *
     * @return bool
     */
  public function execute() {

        /**
         * @var $teacher \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
         */
        $teacher = null;

        /**
         * @var $mail \INSOR\IsCourses2\Helper\StatMailer
         */
        $mail = null;

        // Frontend fuer Frontend-Link zum Statistik-Formular initialisieren
        $this->initFrontend();
        // Initialisierungen
        $this->objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
        $this->contentObjectRenderer = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer');
        $this->coursesRepository = $this->objectManager->get('INSOR\IsCourses2\Domain\Repository\CoursesRepository');
        $this->teachersRepository = $this->objectManager->get('INSOR\IsCourses2\Domain\Repository\TeachersRepository');

        // TODO Sprach-Array via Typoscript konfigurierbar machen
        $lang_array = array (
          0 => 'de',
          1 => 'en',
          2 => 'fr',
          3 => 'it'
        );

        // Kurse auslesen nach Kriterium Statistik-Mail
        $courses = $this->coursesRepository->findStatMailCourses();

        // Loop ueber alle Kurse. $uid ist != uid vom Kurs, sondern Array-Key (0,1,2,3)
        foreach ($courses as $uid => $course) {
            $teacher = null;
            $isAlternativeTeacherEmail = null;
            $feusers = null;
            $statsForm = null;
            $formlink = null;
            $mail = null;
            $teacher_email = null;
            $teacher_name = null;

            //Standard-Wert: Alternative wird im Normalfall nicht gebraucht, wenn Teacher vorhanden
            if ($course['course_teacher_email'] != '') {
                $isAlternativeTeacherEmail = 1;
            } else {
                $isAlternativeTeacherEmail = 0;
            }

            // Teacher abfrage gar nicht erst machen, wenn Alternativ-Email schon gegeben
            if ($isAlternativeTeacherEmail == 0) {
                // Wenn Fe-User-ID des Kurses = 0 oder null, Kurs ueberspringen
                if (!$course['fe_user_id']) {
                    $this->coursesRepository->writeToCourseLog(
                      "Statistik-Mail zu KursID " . $course['course_uid'] . " wurde nicht versendet: kein Teacher zugewiesen, keine alternative Email vorhanden.",
                      $courses[$uid]['course_uid']
                    );
                    $this->log('statmailer_task',
                      "Statistik-Mail zu KursID " . $course['course_uid'] . " wurde nicht versendet: kein Teacher zugewiesen, keine alternative Email vorhanden.");
                    continue;
                } else {
                    // Fe-User-ID des Kurses im Repository suchen, ansonsten ueberspringen
                    // Feld "keine_benachrichtigungen": Wenn gesetzt, darf der FE-User kein Mail erhalten
                    $teacher = $this->teachersRepository->findByUid(intval($course['fe_user_id']));
                    if ($teacher === false) {
                        $this->coursesRepository->writeToCourseLog(
                          "Statistik-Mail zu KursID " . $course['course_uid'] . " wurde nicht versendet: TeacherID " . $course['fe_user_id'] . " ist inaktiv oder geloescht.",
                          $courses[$uid]['course_uid']
                        );
                        $this->log('statmailer_task',
                          "Statistik-Mail zu KursID " . $course['course_uid'] . " wurde nicht versendet: TeacherID " . $course['fe_user_id'] . " ist inaktiv oder geloescht.");
                        continue;
                    } else {
                        if (intval($teacher['keine_benachrichtigungen']) == 1) {
                            $this->coursesRepository->writeToCourseLog(
                              "Statistik-Mail zu KursID " . $course['course_uid'] . " wurde nicht versendet: TeacherID " . $course['fe_user_id'] . " darf nicht benachrichtigt werden.",
                              $courses[$uid]['course_uid']
                            );
                            $this->log('statmailer_task',
                              "Statistik-Mail zu KursID " . $course['course_uid'] . " wurde nicht versendet: TeacherID " . $course['fe_user_id'] . " darf nicht benachrichtigt werden.");
                            continue;
                        } else {
                            // Email Check
                            if (!$this->checkEmail($teacher['email'])) {
                                $this->coursesRepository->writeToCourseLog(
                                  "Statistik-Mail zu KursID " . $course['course_uid'] . " wurde nicht versendet: TeacherID " . $course['fe_user_id'] . " hat keine gueltige Email-Adresse.",
                                  $courses[$uid]['course_uid']
                                );
                                $this->log('statmailer_task',
                                  "Statistik-Mail zu KursID " . $course['course_uid'] . " wurde nicht versendet: TeacherID " . $course['fe_user_id'] . " hat keine gueltige Email-Adresse.");
                                continue;
                            } else {
                                $this->coursesRepository->writeToCourseLog(
                                  "Statistik-Mail zu KursID " . $course['course_uid'] . " verwendet Fe-User-Email: " . $teacher['email'] . " TeacherID " . $course['fe_user_id'],
                                  $courses[$uid]['course_uid']
                                );
                                $this->log('statmailer_task',
                                  "Statistik-Mail zu KursID " . $course['course_uid'] . " verwendet Fe-User-Email: " . $teacher['email'] . " TeacherID " . $course['fe_user_id']);
                            }
                        }
                    }
                }
            }

            // Statistik Eingabemaske herausfinden, oder Default nehmen
            if ($course['owner']) {
                $feusers = $this->coursesRepository->getFeuserRootline($course['owner']);

                $statsForm = $this->coursesRepository->getConfiguredStatsForm($course['course_uid'], $feusers);
                $courses[$uid]['statsFormPid'] = $statsForm;
            }

            $courses[$uid]['statsFormPid'] = ($statsForm) ?: $this->defaultStatsFormPid;
            $courses[$uid]['statsFormHmac'] = sha1($courses[$uid]['course_uid'] . "-{$this->secret}");

            // Formular-Link erstellen fuer Statistik-Formular (Muss hier gemacht werden, da sonst Backend-Link generiert wird)
            $formlink = $this->createFrontendFormLink($courses[$uid]['statsFormPid'], $courses[$uid]['statsFormHmac'],
              $courses[$uid]['course_uid'], $lang_array);

            // StatMailer Objekt erstellen
            $mail = $this->objectManager->get('INSOR\IsCourses2\Helper\StatMailer');
            $error = 0;
            $teacher_email = '';
            $teacher_name = 'Dozent';
            if (($teacher['name'] != '' && $teacher['name'] != null) && ($teacher['email'] != '' && $teacher['email'] != null && $this->checkEmail($teacher['email']))) {
                $teacher_email = $teacher['email'];
                $teacher_name = $teacher['name'];
            } else {
                if ($course['course_teacher_email'] != '' && $course['course_teacher_email'] != null) {
                    $teacher_email = $course['course_teacher_email'];
                    $teacher_name = ($course['course_teacher_text'] == '') ? 'Dozent' : $course['course_teacher_text'];
                    $this->coursesRepository->writeToCourseLog(
                      "Statistik-Mail zu KursID " . $course['course_uid'] . " verwendet Alternativ-Email: " . $course['course_teacher_email'],
                      $courses[$uid]['course_uid']
                    );
                    $this->log('statmailer_task',
                      "Statistik-Mail zu KursID " . $course['course_uid'] . " verwendet Alternativ-Email: " . $course['course_teacher_email']);
                } else {
                    $error = 1;
                }
            }

            if ($error == 0) {
                /**
                 * Siehe Parameter der Funktion fuer erleuterung.
                 * lang Parameter auf de, weil vorerst alle Sprachen in 1 Template (Keine Abfrage der Kurse auf Sprache)
                 */

                //createMail($template, $lang, $sender, $receiver, $subject,$name,$course,$bcc = array(), $formlink = array())
                if ($this->checkEmail($teacher_email) && $teacher_name != null && $teacher_name != '') {
                    $mail->createMail(
                      'scheduler',
                      'de',
                      ['noreply@elternbildung.ch' => 'Elternbildung Statistik'],
                      [$teacher_email => $teacher_name],
                      'Elternbildung CH: bitte Kursstatistik nachführen / Formation des parents CH: saisir la statistique de votre cours',
                      $teacher['name'],
                      $courses[$uid],
                      [
                        'webmaster@insor.ch' => 'INSOR Elternbildung Statistik-Scheduler BCC'
                      ],
                      $formlink
                    );
                    // 'statistik@elternbildung.ch' => 'Statistik Elternbildung CH'
                } else {
                    $this->coursesRepository->writeToCourseLog(
                      "Statistik-Mail zu KursID " . $course['course_uid'] . " wurde nicht versendet: $teacher_email ist keine gueltige Email-Adresse",
                      $course['course_uid']
                    );
                    $this->log('statmailer_task',
                      "Statistik-Mail zu KursID " . $course['course_uid'] . " wurde nicht versendet: $teacher_email ist keine gueltige Email-Adresse");
                }
            }

            // Wenn FE-User-ID gegeben, dann sende Mail
            if ($error == 0) {
                echo 'hello';
                // Wenn Mail gesendet, schreibe in Kurs-Log, schreibe timestamp in stats_reminder_sent, schreibe lokales log.
                $isSent = $mail->sendMail();
                if ($isSent) {
                    // In Log-Tabelle des Kurses schreiben
                    if ($isAlternativeTeacherEmail === 1) {
                        $this->coursesRepository->writeToCourseLog(
                          "Statistik-Mail zu KursID " . $course['course_uid'] . ": Alternativ-Mail verwendet; Email wurde an teacher_email: " . $course['course_teacher_email'] . " versendet.",
                          $courses[$uid]['course_uid']
                        );
                        // In Log-Datei auf Datentraeger schreiben
                        $this->log('statmailer_task',
                          "Statistik-Mail zu KursID " . $course['course_uid'] . ": Alternativ-Mail verwendet; Email wurde an teacher_email: " . $course['course_teacher_email'] . " versendet.");
                    } else {
                        $this->coursesRepository->writeToCourseLog(
                          'Statistik-Mail zu KursID ' . $courses[$uid]['course_uid'] . ' versendet an: ' . $teacher['email'],
                          $courses[$uid]['course_uid']
                        );
                        // In Log-Datei auf Datentraeger schreiben
                        $this->log('statmailer_task',
                          'Statistik-Mail zu KursID ' . $courses[$uid]['course_uid'] . ' versendet an: ' . $teacher['email']);
                    }

                    // stats_reminder_sent des Kurses mit Timestamp ueberschreiben.
                    $this->coursesRepository->writeStatsReminderSent(
                      $courses[$uid]['course_uid']
                    );
                } else {
                    // Mail-Error ins log schreiben, falls PHP-Fehler
                    $this->coursesRepository->writeToCourseLog(
                      "Statistik-Mail zu KursID " . $course['course_uid'] . " wurde nicht versendet: Mailing-Error",
                      $course['course_uid']
                    );

                    // In Log-Datei auf Datentraeger schreiben
                    $this->log('statmailer_task',
                      "Statistik-Mail zu KursID " . $course['course_uid'] . " wurde nicht versendet: Mailing-Error");
                }
            } else {
                $this->coursesRepository->writeToCourseLog(
                  "Statistik-Mail zu KursID " . $course['course_uid'] . " wurde nicht versendet: Keine Gueltige Email-Adresse im Kurs vergeben. course_teacher_email: '" . $course['course_teacher_email'] ."', teacher['email']: '". $teacher['email'] ."'",
                  $course['course_uid']
                );
                $this->log('statmailer_task',
                  "Statistik-Mail zu KursID " . $course['course_uid'] . " wurde nicht versendet: Keine Gueltige Email-Adresse im Kurs vergeben. course_teacher_email: '" . $course['course_teacher_email'] ."', teacher['email']: '". $teacher['email'] ."'");
            }
        }

        return true;
    }

    /**
     * @param $statsFormPid
     * @param $statsFormHmac
     * @param $course_uid
     * @param $lang_array
     * @return array
     */
  private function createFrontendFormLink($statsFormPid,$statsFormHmac,$course_uid,$lang_array) {

        $formlink = array();

        foreach ($lang_array as $lang_key => $lang) {
            $linkConf = array(
              'parameter' => $statsFormPid,
        'additionalParams' => \TYPO3\CMS\Core\Utility\GeneralUtility::implodeArrayForUrl(NULL, array(
                'tx_iscourses2_courses[hmac]' => $statsFormHmac,
                'tx_iscourses2_courses[course_uid]' => $course_uid,
                'tx_powermail_pi1[field][interne_id]' => $course_uid,
                'tx_iscourses2_courses[action]' => 'stats',
                'tx_iscourses2_courses[controller]' => 'Courses',
                'L' => $lang_key
              )),
              'linkAccessRestrictedPages' => 1
            );

            $formlink[$lang_key] = $this->contentObjectRenderer->typolink_URL($linkConf);
        }

//    DebuggerUtility::var_dump($formlink);

        return $formlink;
    }

    /*
     * Minimales Frontend initialisieren fuer Frontend-Aktionen (Wie links)
     * Standard-Code, keine Details noetig.
     */
  private function initFrontend() {
        $id = 1;
    $type = 0;
        if (!is_object($GLOBALS['TT'])) {
            $GLOBALS['TT'] = new \TYPO3\CMS\Core\TimeTracker\TimeTracker;
            $GLOBALS['TT']->start();
        }
    $GLOBALS['TSFE'] = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Frontend\\Controller\\TypoScriptFrontendController',  $GLOBALS['TYPO3_CONF_VARS'], $id, $typeNum);
        $GLOBALS['TSFE']->connectToDB();
        $GLOBALS['TSFE']->initFEuser();
        $GLOBALS['TSFE']->determineId();
        $GLOBALS['TSFE']->initTemplate();
        $GLOBALS['TSFE']->getConfigArray();

        if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('realurl')) {
            $rootline = \TYPO3\CMS\Backend\Utility\BackendUtility::BEgetRootLine($id);
            $host = \TYPO3\CMS\Backend\Utility\BackendUtility::firstDomainRecord($rootline);
            $_SERVER['HTTP_HOST'] = $host;
        }
    }

    /**
     * Ueberpruefung ob String eine gueltige Email-Adresse
     *
     * @param $email
     * @return bool
     */
    function checkEmail($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Log-Funktion um in Log-File auf Datentraeger zu schreiben
     *
     * @param $facility
     * @param $msg
     */
    private function Log($facility, $msg)
    {
        $filename = $facility . ".log";
        if (strpos(getcwd(), 'typo3') !== false) {
            $filename = '../../' . $filename;
        }
        $date = date("Y-m-d H:i:s");
        file_put_contents($filename, $date . "   " . $msg . "\n", FILE_APPEND);
    }
}

?>
