// arrayobjekt für filternamen und id der text-/selectboxen
var filters = {
  'filter_kurskategorie': '#filter_92',
  'filter_region': '#filter_93',
  'filter_betreuungsform': '#filter_94',
  'filter_textsuche': '#textfilter',
  'filter_preis_mitglied': '#subv_mitglied',
  'filter_preis_betreuungsform': '#subv_betrform',
  'filter_preis_kantonarbeitsort': '#subv_kanton',
  'filter_ausbildungsbereich': '#filter_5',
  'filter_beruf': '#filter_24'
};

function doCategoryFilter() {
  doFilter();
}
var timer1 = null;
function doTextFilter(thisvalue) {
  clearTimeout(timer1);
  timer1 = setTimeout(function() {
    doFilter();
  }, 500);
}

// Kombinierte Filter-Funktion Kategorien + Text
function doFilter() {

  // Alle Zeilen verstecken
  $('.tx_iscourses2 .datarow').hide();

  // Jede Zeile durchgehen und anzeigen, wenn sie alle Kategorien erfüllt
  $('.tx_iscourses2 .datarow').each(function() {
    var row = $(this);
    var show = true;
    $('.filterelement.select option:selected').each(function() {
      var a = (''+row.data('categories')).split(',');
      var b = ''+$(this).val();
      if ((b != '') && (a.indexOf(b) == -1)) {
        show = false;
      }
    });
    if (show) row.show();
  });

  // Über die angezeigten Zeilen jetzt noch den Textfilter lassen
  if($('#textfilter').val()) {
    var swords = $('#textfilter').val().toLowerCase().split(' ');
    $('.tx_iscourses2 .datarow:visible').each(function(){
      var row = $(this);
      var show = true;
      $(swords).each(function() {
        thisvalue = this;
        if (row.text().toLowerCase().indexOf(thisvalue) == -1) {
          show = false;
        }
      });
      if (!show) row.hide();
    });
  }

  // Odd / Even nummerierung
  $('.tx_iscourses2 .datarow:visible').each(function(index){
    var row = $(this);
    if(index % 2) {
      row.addClass('odd').removeClass('even');
    } else {
      row.removeClass('odd').addClass('even');
    }
  });
  setUrlQueries();
}

/**
 *  URL Query-Parameter updaten
 */
function setUrlQueries() {
  for (let key of Object.keys(filters)) {
    if ($(filters[key]).val() != null) {
      updateQueryStringParam(key, $(filters[key]).val());
    }
  }
}

// Füllen der Auswahl bei reload
$("document").ready(function(){
  let searchParams = new URL('https://' +location.host + '/' + document.location.search).searchParams;
  for (let key of Object.keys(filters)) {
    // Element befüllen falls dieses existiert
    if ($(filters[key]).val() != null) {
      $($(filters[key])).val(searchParams.get(key));
    }
  }
  // Filter nach delay ausführen (delay benötigt für mobile)
  setTimeout(function () { doFilter(); }, 10);
});

// Kurs-Textfeldveränderung
$("document").ready(function(){
  $('.filterelement.textsearch input').on('input',function(e){
    doTextFilter($(this).val().toLowerCase());
  });

  $('.filterelement').on('keyup keypress', function(e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) {
      e.preventDefault();
      return false;
    }
  });
});

function updateQueryStringParam(key, value) {

  var baseUrl = [location.protocol, '//', location.host, location.pathname].join(''),
      urlQueryString = document.location.search,
      newParam = key + '=' + value,
      params = '?' + newParam;

  // If the "search" string exists, then build params from it
  if (urlQueryString) {
    var updateRegex = new RegExp('([\?&])' + key + '[^&]*');
    var removeRegex = new RegExp('([\?&])' + key + '=[^&;]+[&;]?');

    if( typeof value == 'undefined' || value == null || value == '' ) { // Remove param if value is empty
      params = urlQueryString.replace(removeRegex, "$1");
      params = params.replace( /[&;]$/, "" );

    } else if (urlQueryString.match(updateRegex) !== null) { // If param exists already, update it
      params = urlQueryString.replace(updateRegex, "$1" + newParam);

    } else { // Otherwise, add it to end of query string
      params = urlQueryString + '&' + newParam;
    }
  }

  // no parameter was set so we don't need the question mark
  params = params == '?' ? '' : params;

  window.history.replaceState({}, "", baseUrl + params);
};
