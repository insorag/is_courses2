<?php

if (!isset($GLOBALS['TCA']['fe_users']['ctrl']['type'])) {
	if (file_exists($GLOBALS['TCA']['fe_users']['ctrl']['dynamicConfigFile'])) {
		require_once($GLOBALS['TCA']['fe_users']['ctrl']['dynamicConfigFile']);
	}
	// no type field defined, so we define it here. This will only happen the first time the extension is installed!!
	$GLOBALS['TCA']['fe_users']['ctrl']['type'] = 'tx_extbase_type';
	$tempColumnstx_iscourses2_fe_users = array();
	$tempColumnstx_iscourses2_fe_users[$GLOBALS['TCA']['fe_users']['ctrl']['type']] = array(
		'exclude' => 1,
		'label'   => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2.tx_extbase_type',
		'config' => array(
			'type' => 'select',
			'renderType' => 'selectSingle',
			'items' => array(
				array('Teachers','Tx_IsCourses2_Teachers')
			),
			'default' => 'Tx_IsCourses2_Teachers',
			'size' => 1,
			'maxitems' => 1,
		)
	);
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users', $tempColumnstx_iscourses2_fe_users, 1);
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'fe_users',
	$GLOBALS['TCA']['fe_users']['ctrl']['type'],
	'',
	'after:' . $GLOBALS['TCA']['fe_users']['ctrl']['label']
);

$tmp_is_courses2_columns = array(

	'external_id' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_teachers.external_id',
		'config' => array(
			'type' => 'input',
			'size' => 30,
			'eval' => 'trim'
		),
	),
	'themen' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_teachers.themen',
		'config' => array(
			'type' => 'text',
			'cols' => 40,
			'rows' => 15,
			'eval' => 'trim'
		)
	),
	'werdegang' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_teachers.werdegang',
		'config' => array(
			'type' => 'text',
			'cols' => 40,
			'rows' => 15,
			'eval' => 'trim'
		)
	),
	'ist_mitglied' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_teachers.ist_mitglied',
		'config' => array(
			'type' => 'check',
			'default' => 0
		)
	),
	'online_profil_aktiv' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_teachers.online_profil_aktiv',
		'config' => array(
			'type' => 'check',
			'default' => 0
		)
	),
	'bemerkungen' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_teachers.bemerkungen',
		'config' => array(
			'type' => 'text',
			'cols' => 40,
			'rows' => 15,
			'eval' => 'trim'
		)
	),
	'keine_benachrichtigungen' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_teachers.keine_benachrichtigungen',
		'config' => array(
			'type' => 'check',
			'default' => 0
		)
	),
	'parent' => array(
		'exclude' => 1,
		'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_teachers.parent',
		'config' => array(
			'type' => 'select',
			'items' => [
			  ['-- Kein Parent --', 0],
      ],
			'renderType' => 'selectSingle',
			'foreign_table' => 'fe_users',
			'minitems' => 0,
			'maxitems' => 1,
		),
	),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('fe_users',$tmp_is_courses2_columns);

/* inherit and extend the show items from the parent class */

if(isset($GLOBALS['TCA']['fe_users']['types']['0']['showitem'])) {
	$GLOBALS['TCA']['fe_users']['types']['Tx_IsCourses2_Teachers']['showitem'] = $GLOBALS['TCA']['fe_users']['types']['0']['showitem'];
} elseif(is_array($GLOBALS['TCA']['fe_users']['types'])) {
	// use first entry in types array
	$fe_users_type_definition = reset($GLOBALS['TCA']['fe_users']['types']);
	$GLOBALS['TCA']['fe_users']['types']['Tx_IsCourses2_Teachers']['showitem'] = $fe_users_type_definition['showitem'];
} else {
	$GLOBALS['TCA']['fe_users']['types']['Tx_IsCourses2_Teachers']['showitem'] = '';
}
$GLOBALS['TCA']['fe_users']['types']['Tx_IsCourses2_Teachers']['showitem'] .= ',--div--;LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_teachers,';
$GLOBALS['TCA']['fe_users']['types']['Tx_IsCourses2_Teachers']['showitem'] .= 'external_id, themen, werdegang, ist_mitglied, online_profil_aktiv, bemerkungen, keine_benachrichtigungen, parent';

$GLOBALS['TCA']['fe_users']['columns'][$GLOBALS['TCA']['fe_users']['ctrl']['type']]['config']['items'][] = array('LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:fe_users.tx_extbase_type.Tx_IsCourses2_Teachers','Tx_IsCourses2_Teachers');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
	'9.5 braucht hier einen String, also setze ich einen',
	'EXT:/Resources/Private/Language/locallang_csh_.xlf'
);