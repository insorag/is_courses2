<?php

namespace INSOR\IsCourses2\Tests\Unit\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \INSOR\IsCourses2\Domain\Model\Teachers.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class TeachersTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
	/**
	 * @var \INSOR\IsCourses2\Domain\Model\Teachers
	 */
	protected $subject = NULL;

	public function setUp()
	{
		$this->subject = new \INSOR\IsCourses2\Domain\Model\Teachers();
	}

	public function tearDown()
	{
		unset($this->subject);
	}

	/**
	 * @test
	 */
	public function getExternalIdReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getExternalId()
		);
	}

	/**
	 * @test
	 */
	public function setExternalIdForStringSetsExternalId()
	{
		$this->subject->setExternalId('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'externalId',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getThemenReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getThemen()
		);
	}

	/**
	 * @test
	 */
	public function setThemenForStringSetsThemen()
	{
		$this->subject->setThemen('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'themen',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getWerdegangReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getWerdegang()
		);
	}

	/**
	 * @test
	 */
	public function setWerdegangForStringSetsWerdegang()
	{
		$this->subject->setWerdegang('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'werdegang',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getIstMitgliedReturnsInitialValueForBool()
	{
		$this->assertSame(
			FALSE,
			$this->subject->getIstMitglied()
		);
	}

	/**
	 * @test
	 */
	public function setIstMitgliedForBoolSetsIstMitglied()
	{
		$this->subject->setIstMitglied(TRUE);

		$this->assertAttributeEquals(
			TRUE,
			'istMitglied',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getOnlineProfilAktivReturnsInitialValueForBool()
	{
		$this->assertSame(
			FALSE,
			$this->subject->getOnlineProfilAktiv()
		);
	}

	/**
	 * @test
	 */
	public function setOnlineProfilAktivForBoolSetsOnlineProfilAktiv()
	{
		$this->subject->setOnlineProfilAktiv(TRUE);

		$this->assertAttributeEquals(
			TRUE,
			'onlineProfilAktiv',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getBemerkungenReturnsInitialValueForString()
	{
		$this->assertSame(
			'',
			$this->subject->getBemerkungen()
		);
	}

	/**
	 * @test
	 */
	public function setBemerkungenForStringSetsBemerkungen()
	{
		$this->subject->setBemerkungen('Conceived at T3CON10');

		$this->assertAttributeEquals(
			'Conceived at T3CON10',
			'bemerkungen',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getKeineBenachrichtigungenReturnsInitialValueForBool()
	{
		$this->assertSame(
			FALSE,
			$this->subject->getKeineBenachrichtigungen()
		);
	}

	/**
	 * @test
	 */
	public function setKeineBenachrichtigungenForBoolSetsKeineBenachrichtigungen()
	{
		$this->subject->setKeineBenachrichtigungen(TRUE);

		$this->assertAttributeEquals(
			TRUE,
			'keineBenachrichtigungen',
			$this->subject
		);
	}

	/**
	 * @test
	 */
	public function getParentReturnsInitialValueForTeachers()
	{
		$this->assertEquals(
			NULL,
			$this->subject->getParent()
		);
	}

	/**
	 * @test
	 */
	public function setParentForTeachersSetsParent()
	{
		$parentFixture = new \INSOR\IsCourses2\Domain\Model\Teachers();
		$this->subject->setParent($parentFixture);

		$this->assertAttributeEquals(
			$parentFixture,
			'parent',
			$this->subject
		);
	}
}
