<?php
namespace INSOR\IsCourses2\Controller;

/***************************************************************
*
*  Copyright notice
*
*  (c) 2016
*
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 3 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
* TeachersController
*/
class TeachersController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

  /**
  * teachersRepository
  * 
  * @var \INSOR\IsCourses2\Domain\Repository\TeachersRepository
  * @inject
  */
  protected $teachersRepository = NULL;

  /**
  * coursesRepository
  * 
  * @var \INSOR\IsCourses2\Domain\Repository\CoursesRepository
  * @inject
  */
  protected $coursesRepository = NULL;

  /**
  * @var array
  */
  protected $teacher = NULL;

  /**
   * Uebersetzung via locallang
   *
   * @param $sys_string
   * @return NULL|string
   */
  public function translateSystemMessages($sys_string) {
    return \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_iscourses2.'.$sys_string, 'is_courses2');
  }

  /**
  * Init Funktion um Kundenobjekt zu instanzieren
  */
  private function init()
  {
    $GLOBALS['TSFE']->connectToDB();
    $GLOBALS['TSFE']->initFEuser();
    $feuser_uid = intval($GLOBALS['TSFE']->fe_user->user['uid']);
    $this->teacher = $this->teachersRepository->findByUid($feuser_uid);
    if (!count($this->teacher)) {
      $this->addFlashMessage($this->translateSystemMessages('sys_zugriff_verweigert_ausgeloggter_user'), $this->translateSystemMessages('sys_zugriff_verweigert'), \TYPO3\CMS\Core\Messaging\AbstractMessage::ERROR);
      return;
    }
  }

  /**
  * action list
  * 
  * @return void
  */
  public function listAction()
  {
    // Kategorien Filter auslesen
    $categs = $this->settings['filterCategoriesTeacher'];

    // Alle Kursleiter auslesen
    $teachers = $this->teachersRepository->findAll($categs);
    foreach ($teachers as &$teacher) {
      $teacher['profilepic'] = $this->getProfilePicturePath($teacher['uid']);
      $teacher['categs'] = $this->getUserCategoryTree($teacher['uid']);
      $teacher['categstring'] = implode(',', array_keys($this->teachersRepository->getFeuserCategories($teacher['uid'])));
    }

    // Falls keine Kursleiter gefunden, dann entsprechende Template Variable setzen
    if (count($teachers) == 0) {
      $this->view->assign('no_results', 1);
    }

    // Ausgeben aller Kategorien Filter
    foreach ($this->coursesRepository->findCategories($this->settings['feuserAttributesRootnode']) as $uid => $record) {
      $filter[] = array(
        'uid' => $uid,
        'title' => $record['title'],
        'elements' => $this->coursesRepository->findCategories($uid)
      );
    }

    $this->view->assign('filter', $filter);
    $this->view->assign('teachers', $teachers);
  }

  /**
  * action show
  * 
  * @return void
  */
  public function showAction()
  {

    // FE-User UID aus der Request lesen. Falls nicht gesetzt, dann UID des eingeloggten Users
    $feuser_uid = intval($this->request->getArguments()['feuser_uid']);
    if (!$feuser_uid) {
      $this->init();
      $this->view->assign('can_edit', true);
    } else {
      $this->teacher = $this->teachersRepository->findByUid($feuser_uid);
    }

    $this->assignProfileValues();
  }

  /**
  * action edit
  * 
  * @return void
  */
  public function editAction()
  {
    $this->init();
    $this->assignProfileValues();
  }

  /**
  * action update
  * 
  * @return void
  */
  public function updateAction()
  {
    $this->init();
    $uid = intval($this->teacher['uid']);
    $args = $this->request->getArguments();

    // Felder und Kategorien abspeichern
    $this->teachersRepository->updateFeuser($uid, $args['fe_users']);
    $this->teachersRepository->setNewFeuserCategories($uid, $args['categories']);

    // Bild abspeichern
    if ($args['foto']['tmp_name'] && $args['foto']['error'] == 0) {
      $destination = GeneralUtility::getFileAbsFileName("uploads/tx_iscourses2/teacherpics/");
      if(!is_dir($destination)) {
        mkdir($destination, 0755, true);
      }
      move_uploaded_file($args['foto']['tmp_name'], $destination . "profile.$uid.jpg");
      $this->teachersRepository->removeFileReference("teacherpics/profile.$uid.jpg");
    }

    // Logo abspeichern
    if ($args['logo']['tmp_name'] && $args['logo']['error'] == 0) {
      $destination = GeneralUtility::getFileAbsFileName("uploads/tx_iscourses2/teacherpics/");
      if(!is_dir($destination)) {
        mkdir($destination, 0755, true);
      }
      move_uploaded_file($args['logo']['tmp_name'], $destination . "logo.$uid.jpg");
      $this->teachersRepository->removeFileReference("teacherpics/logo.$uid.jpg");
    }

    $this->addFlashMessage($this->translateSystemMessages('sys_aenderungen_erfolgreich_gespeichert'), '', \TYPO3\CMS\Core\Messaging\AbstractMessage::OK);
    $this->redirect('edit');
  }

  private function assignProfileValues() {
    // Kategorien auslesen für Select-Boxen
    $categs = $this->getCategoryTree();

    // Dem FE-User zugewiesene Kategorien
    $assigned_categories = $this->teachersRepository->getFeuserCategories($this->teacher['uid']);
    foreach ($categs as &$categgroup) {
      foreach ($categgroup['elements'] as &$cat) {
        if ($assigned_categories[$cat['uid']]) {
          $cat['selected'] = 'selected';
        }
      }
    }

    // Kategorien mit allen möglichen Werten
    $this->view->assign('categs', $categs);

    // Alle FE-User Felder
    $this->view->assign('teacher', $this->teacher);

    // Das Profilbild
    $this->view->assign('profilepic', $this->getProfilePicturePath($this->teacher['uid']));

    // Das Logobild
    $this->view->assign('logopic', $this->getProfilePicturePath($this->teacher['uid'], 'logo'));

    // Page Title setzen
    $GLOBALS['TSFE']->page['title'] = $this->teacher['name'];

  }

  private function getProfilePicturePath($feuser_uid, $type = 'profile') {
    $filename = "$type.{$feuser_uid}.jpg";
    $relpath = "uploads/tx_iscourses2/teacherpics/";
    if (is_file(GeneralUtility::getFileAbsFileName($relpath.$filename))) {
      return $relpath.$filename;
    } else {
      return $relpath."$type.dummy.jpg";
    }
  }

  /**
   * @return array
   */
  private function getCategoryTree():array
  {
    $categs = array();
    foreach ($this->coursesRepository->findCategories($this->settings['feuserAttributesRootnode']) as $uid => $record) {
      $categs[] = array(
        'uid' => $uid,
        'title' => $record['title'],
        'elements' => $this->coursesRepository->findCategories($uid)
      );
    }
    return $categs;
  }

  /**
   * @param $feuser_uid
   * @return array
   */
  private function getUserCategoryTree($feuser_uid):array
  {
    $categs = array();
    foreach ($this->coursesRepository->findCategories($this->settings['feuserAttributesRootnode']) as $uid => $record) {
      $categs[] = array(
        'uid' => $uid,
        'title' => $record['title'],
        'elements' => $this->teachersRepository->getFeuserCategories($feuser_uid, $uid)
      );
    }
    return $categs;
  }

}