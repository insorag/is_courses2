<?php
namespace INSOR\IsCourses2\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * StatistikFragebogenLink
 */
class StatistikFragebogenLink extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * powermailFormId
     *
     * @var int
     */
    protected $powermailFormId = 0;
    
    /**
     * categoryId
     *
     * @var int
     */
    protected $categoryId = 0;
    
    /**
     * feAdminId
     *
     * @var \INSOR\IsCourses2\Domain\Model\Teachers
     */
    protected $feAdminId = null;
    
    /**
     * Returns the powermailFormId
     *
     * @return int $powermailFormId
     */
    public function getPowermailFormId()
    {
        return $this->powermailFormId;
    }
    
    /**
     * Sets the powermailFormId
     *
     * @param int $powermailFormId
     * @return void
     */
    public function setPowermailFormId($powermailFormId)
    {
        $this->powermailFormId = $powermailFormId;
    }
    
    /**
     * Returns the categoryId
     *
     * @return int $categoryId
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }
    
    /**
     * Sets the categoryId
     *
     * @param int $categoryId
     * @return void
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;
    }
    
    /**
     * Returns the feAdminId
     *
     * @return \INSOR\IsCourses2\Domain\Model\Teachers feAdminId
     */
    public function getFeAdminId()
    {
        return $this->feAdminId;
    }
    
    /**
     * Sets the feAdminId
     *
     * @param \INSOR\IsCourses2\Domain\Model\Teachers $feAdminId
     * @return void
     */
    public function setFeAdminId(\INSOR\IsCourses2\Domain\Model\Teachers $feAdminId)
    {
        $this->feAdminId = $feAdminId;
    }

}