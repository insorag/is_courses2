<?php
namespace INSOR\IsCourses2\Helper;

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class StatMailer {

  /**
   * Email-Objekt zum versenden
   * @var $mail \TYPO3\CMS\Core\Mail\MailMessage
   */
  protected $mail = null;

  /**
   * Template-Objekt fuer den Email-Body
   * @var $mailTemplate \TYPO3\CMS\Fluid\View\StandaloneView
   */
  protected $mailTemplate = null;

  /**
   * @var $objectManager \TYPO3\CMS\Extbase\Object\ObjectManager
   */
  protected $objectManager;

  /**
   * StatMailer constructor.
   *
   * Mail und FluidStandaloneView werden beim instanzieren erstellt
   */
  public function __construct() {
    $this->objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
    $this->mail = $this->objectManager->get('TYPO3\CMS\Core\Mail\MailMessage');
    $this->mailTemplate = $this->objectManager->get('TYPO3\CMS\Fluid\View\StandaloneView');
  }

  /**
   * Initialisierungsfunktion. Nimmt unten stehende Parameter entgegen und erstellt ein Mail
   *
   * @param $template string Kann 'scheduler' oder 'reminder' sein, je nach dem was fuer ein Mail geschickt werden soll.
   * @param $lang string Language Parameter in Form von 'de', 'fr' etc...
   * @param $sender array (addresse, name) Der Absender
   * @param $receiver array (addresse, name) Der Empfaenger
   * @param $subject string (Betreff)
   * @param $name string Name des Empfaengers im Email Template
   * @param $course array Kurs-Array
   * @param $bcc array BCC falls gewünscht
   * @param $formlink array Falls Link nicht in Template generiert werden kann, hier uebergeben (scheduler)
   */
  public function createMail($template, $lang, $sender, $receiver, $subject,$name,$course,$bcc = array(), $formlink = array()) {
    // Create the message
    $this->mail->setSubject($subject);
    $this->mail->setTo($receiver);
    $this->mail->setFrom($sender);

    // If via Parameter uebergeben
    if(count($bcc)>0) {
      $this->mail->setBcc($bcc);
    }

    $this->mail->setFormat('html');
    $this->mail->setContentType('text/html');

    $this->createMailTemplate($template,$lang,$name,$course,$formlink);
  }

  /**
   * Erstellt Email-Body anhand eines Fluid-Templates. Welches verwendet wird haengt von uebergebener
   * Sprache und Template-String ab.
   *
   * @param $template string Kann 'scheduler' oder 'reminder' sein, je nach dem was fuer ein Mail geschickt werden soll.
   * @param $lang string Language Parameter in Form von 'de', 'fr' etc...
   * @param $name string Name des Empfaengers im Email Template
   * @param $course array Kurs-Array
   * @param $formlink array Falls Link nicht in Template generiert werden kann, hier uebergeben (scheduler)
   * @internal param string $formlink Link des Formulars, mit welchem Statistik erfasst werden kann
   */
  private function createMailTemplate($template, $lang,$name,$course,$formlink) {
    switch($template) {
      case 'scheduler':
        $this->mailTemplate->setTemplatePathAndFilename(
          \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('is_courses2','Resources/Private/StatsMail/mailtemplate_'.$lang.'.html'));
        break;
      case 'reminder':
        $this->mailTemplate->setTemplatePathAndFilename(
          \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('is_courses2','Resources/Private/StatsMail/mailtemplate_reminder_'.$lang.'.html'));
        break;
    }

    $this->mailTemplate->setFormat('html');
    $this->mailTemplate->assign('name',$name);
    $this->mailTemplate->assign('formlink',$formlink);
    $this->mailTemplate->assignMultiple($course);
    $this->mail->setBody($this->mailTemplate->render(),'text/html');
  }

  /**
   * Testfunktion um zu checken, ob Body gesetzt ist und mail gesendet werden kann
   *
   * @return string
   */
  public function getMailBody() {
    return $this->mail->getBody();
  }

  /**
   * Testfunktion um zu checken, ob From gesetzt ist und mail gesendet werden kann
   *
   * @return string
   */
  public function getMailFrom() {
    return $this->mail->getFrom();
  }

  /**
   * Sendet vorbereitete Email und liefert isSent() Wert zurueck (boolean)
   *
   * @return bool
   */
  public function sendMail() {
    if($this->mail->getBody() != '' && $this->mail->getFrom() != '') {
      $this->mail->send();
      return $this->mail->isSent();
    }
  }
}

?>