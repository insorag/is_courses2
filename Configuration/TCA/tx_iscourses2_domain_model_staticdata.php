<?php
return array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_staticdata',
		'label' => 'interner_bezeichner',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',

		'enablecolumns' => array(

		),
		'searchFields' => 'interner_bezeichner,stammdatensatz_id,detail_url,anmelde_url,kosten,unterlagen_links,anzeige_bis_enddatum,',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('is_courses2') . 'Resources/Public/Icons/tx_iscourses2_domain_model_staticdata.gif'
	),
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, interner_bezeichner, stammdatensatz_id, detail_url, anmelde_url, kosten, unterlagen_links, anzeige_bis_enddatum',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, interner_bezeichner, stammdatensatz_id, detail_url, anmelde_url, kosten, unterlagen_links, anzeige_bis_enddatum, '),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_iscourses2_domain_model_staticdata',
				'foreign_table_where' => 'AND tx_iscourses2_domain_model_staticdata.pid=###CURRENT_PID### AND tx_iscourses2_domain_model_staticdata.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),

		'interner_bezeichner' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_staticdata.interner_bezeichner',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'stammdatensatz_id' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_staticdata.stammdatensatz_id',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'detail_url' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_staticdata.detail_url',
			'config' => array(
				'type' => 'input',
        'renderType' => 'inputLink',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'anmelde_url' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_staticdata.anmelde_url',
			'config' => array(
				'type' => 'input',
        'renderType' => 'inputLink',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'kosten' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_staticdata.kosten',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			)
		),
		'unterlagen_links' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_staticdata.unterlagen_links',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			)
		),
		'anzeige_bis_enddatum' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_staticdata.anzeige_bis_enddatum',
			'config' => array(
				'type' => 'check',
				'default' => 0
			)
		),
		
	),
);