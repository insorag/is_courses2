<?php
namespace INSOR\IsCourses2\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Teachers
 */
class Teachers extends \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
{

    /**
     * externalId
     *
     * @var string
     */
    protected $externalId = '';
    
    /**
     * On which topics has this person specialised
     *
     * @var string
     */
    protected $themen = '';
    
    /**
     * Short representation of this persons curriculum
     *
     * @var string
     */
    protected $werdegang = '';
    
    /**
     * If this teacher is a member of the organization
     *
     * @var bool
     */
    protected $istMitglied = false;
    
    /**
     * onlineProfilAktiv
     *
     * @var bool
     */
    protected $onlineProfilAktiv = false;
    
    /**
     * bemerkungen
     *
     * @var string
     */
    protected $bemerkungen = '';
    
    /**
     * keineBenachrichtigungen
     *
     * @var bool
     */
    protected $keineBenachrichtigungen = false;
    
    /**
     * parent
     *
     * @var \INSOR\IsCourses2\Domain\Model\Teachers
     */
    protected $parent = null;
    
    /**
     * Returns the externalId
     *
     * @return string $externalId
     */
    public function getExternalId()
    {
        return $this->externalId;
    }
    
    /**
     * Sets the externalId
     *
     * @param string $externalId
     * @return void
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }
    
    /**
     * Returns the themen
     *
     * @return string $themen
     */
    public function getThemen()
    {
        return $this->themen;
    }
    
    /**
     * Sets the themen
     *
     * @param string $themen
     * @return void
     */
    public function setThemen($themen)
    {
        $this->themen = $themen;
    }
    
    /**
     * Returns the werdegang
     *
     * @return string $werdegang
     */
    public function getWerdegang()
    {
        return $this->werdegang;
    }
    
    /**
     * Sets the werdegang
     *
     * @param string $werdegang
     * @return void
     */
    public function setWerdegang($werdegang)
    {
        $this->werdegang = $werdegang;
    }
    
    /**
     * Returns the istMitglied
     *
     * @return bool $istMitglied
     */
    public function getIstMitglied()
    {
        return $this->istMitglied;
    }
    
    /**
     * Sets the istMitglied
     *
     * @param bool $istMitglied
     * @return void
     */
    public function setIstMitglied($istMitglied)
    {
        $this->istMitglied = $istMitglied;
    }
    
    /**
     * Returns the boolean state of istMitglied
     *
     * @return bool
     */
    public function isIstMitglied()
    {
        return $this->istMitglied;
    }
    
    /**
     * Returns the onlineProfilAktiv
     *
     * @return bool $onlineProfilAktiv
     */
    public function getOnlineProfilAktiv()
    {
        return $this->onlineProfilAktiv;
    }
    
    /**
     * Sets the onlineProfilAktiv
     *
     * @param bool $onlineProfilAktiv
     * @return void
     */
    public function setOnlineProfilAktiv($onlineProfilAktiv)
    {
        $this->onlineProfilAktiv = $onlineProfilAktiv;
    }
    
    /**
     * Returns the boolean state of onlineProfilAktiv
     *
     * @return bool
     */
    public function isOnlineProfilAktiv()
    {
        return $this->onlineProfilAktiv;
    }
    
    /**
     * Returns the bemerkungen
     *
     * @return string $bemerkungen
     */
    public function getBemerkungen()
    {
        return $this->bemerkungen;
    }
    
    /**
     * Sets the bemerkungen
     *
     * @param string $bemerkungen
     * @return void
     */
    public function setBemerkungen($bemerkungen)
    {
        $this->bemerkungen = $bemerkungen;
    }
    
    /**
     * Returns the parent
     *
     * @return \INSOR\IsCourses2\Domain\Model\Teachers $parent
     */
    public function getParent()
    {
        return $this->parent;
    }
    
    /**
     * Sets the parent
     *
     * @param \INSOR\IsCourses2\Domain\Model\Teachers $parent
     * @return void
     */
    public function setParent(\INSOR\IsCourses2\Domain\Model\Teachers $parent)
    {
        $this->parent = $parent;
    }
    
    /**
     * Returns the keineBenachrichtigungen
     *
     * @return bool $keineBenachrichtigungen
     */
    public function getKeineBenachrichtigungen()
    {
        return $this->keineBenachrichtigungen;
    }
    
    /**
     * Sets the keineBenachrichtigungen
     *
     * @param bool $keineBenachrichtigungen
     * @return void
     */
    public function setKeineBenachrichtigungen($keineBenachrichtigungen)
    {
        $this->keineBenachrichtigungen = $keineBenachrichtigungen;
    }
    
    /**
     * Returns the boolean state of keineBenachrichtigungen
     *
     * @return bool
     */
    public function isKeineBenachrichtigungen()
    {
        return $this->keineBenachrichtigungen;
    }

}