<?php
if (!defined('TYPO3_MODE')) {
  die ('Access denied.');
}

/*
$GLOBALS['TCA']['tx_iscourses2_domain_model_courses']['columns']['status']['config']['items'] = array(
  array('Pendent', 0),
  array('Freigegeben', 1),
);
*/

$GLOBALS['TCA']['tx_iscourses2_domain_model_courses']['columns']['beschreibung']['config']['rows'] = 8;
$GLOBALS['TCA']['tx_iscourses2_domain_model_courses']['columns']['kurstage']['config']['rows'] = 3;
$GLOBALS['TCA']['tx_iscourses2_domain_model_courses']['columns']['kurs_adresse']['config']['rows'] = 3;
$GLOBALS['TCA']['tx_iscourses2_domain_model_courses']['columns']['unterlagen_links']['config']['rows'] = 3;

$GLOBALS['TCA']['tx_iscourses2_domain_model_courses']['columns']['teacher']['config']['foreign_table_where'] = 'ORDER BY username ASC';


$GLOBALS['TCA']['tx_iscourses2_domain_model_courses']['types'] = array(
  '1' => array('showitem' => 'titel, beschreibung, kosten, dauer, kurstage, kurs_adresse, kursort, detail_url, anmelde_url, unterlagen_links, --div--;Personen, teacher_text, teacher, owner, --div--;Anzeigesteuerung, hidden;;1, start_datum, end_datum, anmeldeschluss_datum, anz_teilnehmer, max_teilnehmer, anzeige_bis_enddatum,--div--;Erweitert, status, external_id, stammdatensatz_id,tx_extbase_type,picture,--div--;Log,log'),
);
