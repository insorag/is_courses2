<?php
  namespace INSOR\IsCourses2\Helper;
  
  class InsorLib {

    /**
     * @var \TYPO3\CMS\Dbal\Database\DatabaseConnection
     */
    protected $db = null;

    public function __construct() {
      $this->db = $GLOBALS['TYPO3_DB'];
    }

    /**
    * @param \TYPO3\CMS\Dbal\Database\DatabaseConnection
    * @param string
    */
    public static function GetFirstField($db, $sql) {
      $result = $db->sql_query($sql);
      $row = $db->sql_fetch_row($result);
      return $row[0];
    }
    
    /**
    * @param \TYPO3\CMS\Dbal\Database\DatabaseConnection
    * @param string
    */
    public static function GetFirstRow($db, $sql) {
      $result = $db->sql_query($sql);
      return $db->sql_fetch_assoc($result) ?: array();
    }
    
    /**
    * @param \TYPO3\CMS\Dbal\Database\DatabaseConnection
    * @param string
    */
    public static function GetFirstColumn($db, $sql) {
      $result = $db->sql_query($sql);
      while ($row = $db->sql_fetch_row($result)) {
        $col[] = $row[0];
      }                
      return $col ?: array();
    }
    
    /**
    * @param \TYPO3\CMS\Dbal\Database\DatabaseConnection
    * @param string
    */
    public static function GetDataset($db, $sql) {
      $result = $db->sql_query($sql);
      while ($row = $db->sql_fetch_assoc($result)) { 
        if ($row['uid']) {
          $rows[$row['uid']] = $row;
        } else {
          $rows[] = $row;
        }
      }                
      return $rows ?: array();
    }


    /**
    * @param \TYPO3\CMS\Dbal\Database\DatabaseConnection
    * @param string
    */
    public static function TranslateUids($db, $table, $lang, $uids) {
      foreach($uids as $uid) {
        $sql = "SELECT uid from $table 
                  where uid = $uid OR (l10n_parent = $uid AND sys_language_uid = $lang) 
                  ORDER BY sys_language_uid desc";
        $uids_translated[$uid] = self::GetFirstField($db, $sql);
      }
      foreach ($uids_translated as $uid) {
        $newuids[] = $uid;
      }
      
      return $newuids ?: array();
    }

    public static function br2space($string)
    {
      return preg_replace('/\<br(\s*)?\/?\>/i', " ", $string);
    }
    
    
    /**
    * @param \TYPO3\CMS\Dbal\Database\DatabaseConnection
    * @param string
    */
    public static function GetDefaultlangUid($db, $table, $uid) {
      $sql = "SELECT CASE WHEN l10n_parent > 0 then l10n_parent else uid end FROM $table WHERE uid = $uid";
      return self::GetFirstField($db, $sql);
      
    }

    /**
    * @param \TYPO3\CMS\Dbal\Database\DatabaseConnection
    * @param string Tablename
    * @param mixed key/value pairs
    * @param integer UID of the record (to update)
    * @param bool Wether the change should be timestamped
    */
    public static function InsertOrUpdate($db, $table, $values, $uid = null, $tstamping = false) {
      foreach ($values as &$el) {
        $el = addslashes($el);
      }

      // UPDATE
      if ($uid) {
        if ($tstamping) $values['tstamp'] = time();
        foreach ($values as $fieldname => $value) {
          $fieldvalues[] = " $fieldname = '$value' ";
        }
        $fieldvalues = implode(',', $fieldvalues);
        $sql = "UPDATE $table SET $fieldvalues WHERE uid = $uid";
        $db->sql_query($sql);

        return $uid;
      
      // INSERT
      } else {
        if ($tstamping) $values['crdate'] = time();
        $fieldnames = implode(',', array_keys($values));

        foreach ($values as $key => $element) {
          $values[$key] = "'$element'";
        }
        $fieldvalues = implode(',', $values);
        
        $sql = "INSERT INTO $table ($fieldnames) VALUES ($fieldvalues)";
        $db->sql_query($sql);
        return ($db->sql_insert_id()) ?: $uid;
      }
    }
    
    /**
    * Kovertiert einen Datumstring von einem bestimmten Format in ein anderes
    * 
    * @param string $input
    * @param string $inputformat
    * @param string $returnformat
    */
    public static function ConvertDateString($input, $inputformat, $returnformat) {
      return date_format(date_create_from_format($inputformat, $input), $returnformat);
    }
    
    /**
    * Encodiert eine Datei wenn nötig nach UTF8
    * 
    * @param string $filename
    */
    public static function FileEncodeUTF8($filename) {
      $output = false;
      
      $input = file_get_contents($filename);
      
      if (mb_detect_encoding($input) != 'UTF-8') {
        $output = mb_convert_encoding( $input, 'UTF-8' ); 
      }
      
      if ($output) {
        file_put_contents($filename, $output);
      }
    }

    /**
     * Kürzt einen String auf eine bestimmte Anzahl Zeichen
     *
     * @param $originalstring
     * @return string
     */
    public static function ShortenString($originalstring) {
      $originalstring = str_replace(array('<br>', '<br/>', '<br />'), array(' ', ' ', ' '), $originalstring);
      $originalstring = strip_tags($originalstring, null);
      if (strlen($originalstring) > 160) {
        $shortened = preg_replace('/\s+?(\S+)?$/', '', substr($originalstring, 0, 160)) . ' ...';
      } else {
        $shortened = $originalstring;
      }
      return $shortened;
    }

    /**
    * Schreibt eine Nachricht in ein Logfile, welches sich relativ zum TYPO3 Root befindet
    * 
    * @param mixed $filename
    * @param mixed $msg
    */
    public static function Log($facility, $msg) {
      $filename = $facility . ".log";
      
      if (strpos(getcwd(), 'typo3') !== false) {
        $filename = '../'.$filename;
      } 
      
      $date = date("Y-m-d H:i:s");
      
      file_put_contents($filename, $date."   ".$msg."\n", FILE_APPEND);
    }
    
    /**
    * Liest eine CSV Datei ein, und gibt ein UTF-8 codiertes, multidimensionales, assoziatives Array zurück
    * 
    * @param string $filename Name und Pfad der CSV Datei
    * @param string $keyfield Feld, welches als Array-Key verwendet werden soll
    */
    public static function FileGetCSV($filename, $keyfield, $excludeFromEscape = null) {
      $zeilen = array();
      
      $excludeFromEscape = ($excludeFromEscape) ? array_flip($excludeFromEscape) : array();
      
      if (strpos(getcwd(), 'typo3') !== false) {
        // Aufruf aus dem Backend (Scheduler)
        $handle = fopen('../'.$filename, 'r');
      } else {
        // Aufruf per eID
        $handle = fopen($filename, 'r');
      }
      
      $rowcounter = 0;
      while (($zeile = fgetcsv($handle)) !== FALSE) {
        if (++$rowcounter == 1) {
          foreach ($zeile as $coltitle) {
            $idx[] = strtolower($coltitle);
          }
          continue;
        }
        
        $neue_zeile = array();
        foreach ($zeile as $key => $value) {
          if (!isset($excludeFromEscape[$idx[$key]])) {
            $value = addslashes($value);
          }
          $value = str_replace('\0', '', $value);
          $neue_zeile[$idx[$key]] = utf8_encode($value);
        }
        
        if ($keyfield) {
          $zeilen[$neue_zeile[$keyfield]] = $neue_zeile;
        } else {
          $zeilen[] = $neue_zeile;
        }
        
      }
      return $zeilen;
    }

    /**
     * Liest eine Excel Datei ein, und gibt ein UTF-8 codiertes, multidimensionales Array zurück
     *
     * @param string $filename Name und Pfad der CSV Datei
     * @return array
     */
    public static function FileGetExcel($filename) {
      $zeilen = array();

      $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
      $excelDocument = $objReader->load($filename);
      $excelSheet = $excelDocument->getActiveSheet();

      $rowcounter = 0;
      foreach ($excelSheet->getRowIterator() as $row) {
        $cellIterator = $row->getCellIterator();

        // Erste Zeile enthält Überschriften
        if (++$rowcounter == 1) {
          foreach ($cellIterator as $cell) {
            $idx[] = $cell->getValue();
          }
          continue;

        // Restliche Zeilen enthalten Daten
        } else {
          $neue_zeile = array();
          $colnr = -1;
          foreach ($cellIterator as $cell) {
            $colnr++;
            $value = addslashes($cell->getValue());
            $value = str_replace('\0', '', $value);
            $neue_zeile[$idx[$colnr]] = $value;
          }
        }

        $zeilen[] = $neue_zeile;

      }
      return $zeilen;
    }

    /**
     * Schreibt eine Excel-Datei anhand eines gegebenen Arrays
     * Gibt den Pfad zum generierten Excel-File
     *
     * @param $valuesarray array Assoziatives Array fuer Excel-Spalten
     * @param $headerarray array Array für Header-Spalte
     * @param $filename string Filename
     * @param $feuser_id int feuser_id, zur eindeutigen Generierung eines Namens notwendig
     * @param string $creator Excel-Creator
     * @param string $title Excel-title
     * @param string $subject Excel-subject
     * @param string $description Excel-description
     *
     * @return string Pfad zum abgelegten Excel
     */
    public static function writeExcel($valuesarray, $headerarray, $filename, $feuser_id = 0, $creator = null,
                                      $title = null, $subject = null, $description = null) {
      // Neues Excel-Objekt (ist dann File) erstellen
      $objPHPExcel = new \PHPExcel();

      // Titel und allgemeines setzen
      if($creator) {
        $objPHPExcel->getProperties()->setCreator($creator);
        $objPHPExcel->getProperties()->setLastModifiedBy($creator);
      }
      if($title) $objPHPExcel->getProperties()->setTitle($title);
      if($subject) $objPHPExcel->getProperties()->setSubject($subject);
      if($description) $objPHPExcel->getProperties()->setDescription($description);

      // Erstes Excell-Worksheet in Excel-File erstellen und aktivieren
      $objPHPExcel->setActiveSheetIndex(0);

      // Zuerst alle Werte die man erhalten hat abfuellen
      // Wichtige Erklaerung zu fromArray:
      /**
       * fromArray schreibt das ganze Excel-Sheet neu, alles vorher drin war wird ueberschrieben.
       * Man kann noch einen Startpunkt angeben, zur Zeit aber nicht implementiert.
       *
       * fromarray arbeitet die 1. Dimension als Rows ab, 2. Dimension als Spalte
       */
      $objPHPExcel->getActiveSheet()->fromArray($valuesarray);

      // Neue Row zuoberst fuer die Header-Spalte einfuegen
      $objPHPExcel->getActiveSheet()->insertNewRowBefore(1, 1);

      // Ueber Header-Array loopen und alle Zellen der obersten Reihe abfuellen
      $header_counter = 0;
      foreach($headerarray as $field) {
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($header_counter,1,$field);
        $header_counter++;
      }

      // Excell-Datei schreiben und in typo3temp Ordner ablegen. Dafür zuerst Writer erstellen.
      $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
      // Name generieren
      $name = '/typo3temp/is_helper/'.$filename.'-'.$feuser_id.'-'.time().'.xlsx';
      // Falls insor Ordner nicht existiert, erstellen
      if (!file_exists(getcwd().'/typo3temp/is_helper')) {
        mkdir(getcwd().'/typo3temp/is_helper', 0777, true);
      }

      // Excel-File schreiben
      $objWriter->save(getcwd().$name);

      return $name;
    }

    /**
     * Nimmt einen Dateinamen entgegen, und verlinkt diese Datei in FAL-Manier mit einem Datensatz einer
     * bestimmten Tabelle.
     * @param $file Dateiname mit relativem Pfad
     * @param $tablename Zu referenzierende Tabelle (z.B. fe_users)
     * @param $fieldname Zu referenzierendes Feld (z.B. foto)
     * @param $uid Zu referenzierender Record
     * @param int $pid |int Auf welcher Page soll die File-Reference abgelegt werden
     * @return int UID der neuen File-Referenz
     */
    /*
    public function LinkFileFAL($file, $tablename, $fieldname, $uid, $replaceAll = false, $pid = 0) {
      if ($replaceAll) {
        $sql = "DELETE FROM sys_file_reference WHERE uid_foreign = $uid AND tablenames = '$tablename' AND fieldname = '$fieldname' AND table_local = 'sys_file'";
        $this->sql_query($sql);
      }

      $filename = end(explode('/', $file));
      $sha1 = sha1_file($file);

      $sql = "INSERT INTO sys_file (storage, type, identifier, name, sha1) VALUES (1, 2, '$file', '$filename', '$sha1')";
      $this->db->sql_query($sql);
      $file_id = $this->db->sql_insert_id();

      $sql = "INSERT INTO sys_file_reference (pid, uid_local, uid_foreign, tablenames, fieldname, table_local) VALUES (
        $pid, $file_id, $uid, '$tablename', '$fieldname', 'sys_file'
      )";
      $this->db->sql_query($sql);

      return $this->db->sql_insert_id();
    }
    */
  }

?>