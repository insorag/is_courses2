<?php
namespace INSOR\IsCourses2\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Staticdata
 */
class Staticdata extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * internerBezeichner
     *
     * @var string
     * @validate NotEmpty
     */
    protected $internerBezeichner = '';
    
    /**
     * stammdatensatzId
     *
     * @var string
     */
    protected $stammdatensatzId = '';
    
    /**
     * detailUrl
     *
     * @var string
     */
    protected $detailUrl = '';
    
    /**
     * anmeldeUrl
     *
     * @var string
     */
    protected $anmeldeUrl = '';
    
    /**
     * kosten
     *
     * @var string
     */
    protected $kosten = '';
    
    /**
     * unterlagenLinks
     *
     * @var string
     */
    protected $unterlagenLinks = '';
    
    /**
     * anzeigeBisEnddatum
     *
     * @var bool
     */
    protected $anzeigeBisEnddatum = false;
    
    /**
     * Returns the anmeldeUrl
     *
     * @return string $anmeldeUrl
     */
    public function getAnmeldeUrl()
    {
        return $this->anmeldeUrl;
    }
    
    /**
     * Sets the anmeldeUrl
     *
     * @param string $anmeldeUrl
     * @return void
     */
    public function setAnmeldeUrl($anmeldeUrl)
    {
        $this->anmeldeUrl = $anmeldeUrl;
    }
    
    /**
     * Returns the internerBezeichner
     *
     * @return string $internerBezeichner
     */
    public function getInternerBezeichner()
    {
        return $this->internerBezeichner;
    }
    
    /**
     * Sets the internerBezeichner
     *
     * @param string $internerBezeichner
     * @return void
     */
    public function setInternerBezeichner($internerBezeichner)
    {
        $this->internerBezeichner = $internerBezeichner;
    }
    
    /**
     * Returns the kosten
     *
     * @return string $kosten
     */
    public function getKosten()
    {
        return $this->kosten;
    }
    
    /**
     * Sets the kosten
     *
     * @param string $kosten
     * @return void
     */
    public function setKosten($kosten)
    {
        $this->kosten = $kosten;
    }
    
    /**
     * Returns the stammdatensatzId
     *
     * @return string $stammdatensatzId
     */
    public function getStammdatensatzId()
    {
        return $this->stammdatensatzId;
    }
    
    /**
     * Sets the stammdatensatzId
     *
     * @param string $stammdatensatzId
     * @return void
     */
    public function setStammdatensatzId($stammdatensatzId)
    {
        $this->stammdatensatzId = $stammdatensatzId;
    }
    
    /**
     * Returns the detailUrl
     *
     * @return string detailUrl
     */
    public function getDetailUrl()
    {
        return $this->detailUrl;
    }
    
    /**
     * Sets the detailUrl
     *
     * @param string $detailUrl
     * @return void
     */
    public function setDetailUrl($detailUrl)
    {
        $this->detailUrl = $detailUrl;
    }
    
    /**
     * Returns the unterlagenLinks
     *
     * @return string $unterlagenLinks
     */
    public function getUnterlagenLinks()
    {
        return $this->unterlagenLinks;
    }
    
    /**
     * Sets the unterlagenLinks
     *
     * @param string $unterlagenLinks
     * @return void
     */
    public function setUnterlagenLinks($unterlagenLinks)
    {
        $this->unterlagenLinks = $unterlagenLinks;
    }
    
    /**
     * Returns the anzeigeBisEnddatum
     *
     * @return bool $anzeigeBisEnddatum
     */
    public function getAnzeigeBisEnddatum()
    {
        return $this->anzeigeBisEnddatum;
    }
    
    /**
     * Sets the anzeigeBisEnddatum
     *
     * @param bool $anzeigeBisEnddatum
     * @return void
     */
    public function setAnzeigeBisEnddatum($anzeigeBisEnddatum)
    {
        $this->anzeigeBisEnddatum = $anzeigeBisEnddatum;
    }
    
    /**
     * Returns the boolean state of anzeigeBisEnddatum
     *
     * @return bool
     */
    public function isAnzeigeBisEnddatum()
    {
        return $this->anzeigeBisEnddatum;
    }

}