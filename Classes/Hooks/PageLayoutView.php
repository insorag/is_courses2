<?php
  namespace INSOR\IsCourses2\Hooks;

  class PageLayoutView implements \TYPO3\CMS\Backend\View\PageLayoutViewDrawItemHookInterface {

    /**
     * Preprocesses the preview rendering of a content element.
     *
     * @param PageLayoutView $parentObject Calling parent object
     * @param boolean $drawItem Whether to draw the item using the default functionalities
     * @param string $headerContent Header content
     * @param string $itemContent Item content
     * @param array $row Record row of tt_content
     * @return void
     */
    public function preProcess(\TYPO3\CMS\Backend\View\PageLayoutView &$parentObject, &$drawItem, &$headerContent, &$itemContent, array &$row) {

      if ($row['list_type'] !== 'iscourses2_courses') return;

      $drawItem = false;
      $headerContent = '<b>Kurse und Veranstaltungen' . "</b><br>";

      $flexform = $row['pi_flexform'];

      //fetch the xml fleform value and get the value (field[0] - this depends on your own flexform)
      //see article on this page "XML Dateien in Extbase"
      $xml = simplexml_load_string($flexform);
      $config = json_decode(json_encode($xml), true)['data']['sheet']['language']['field'];

      foreach($config as $configitem) {
        if (is_array($configitem)) {
          $settings[$configitem['@attributes']['index']] = $configitem['value'];
        }
      }

      switch(explode(';',$settings['switchableControllerActions'])[0]) {
        case 'Courses->list': $itemContent .= "Auflistung Kurse<br>"; break;
        case 'Courses->show': $itemContent .= "Detailanzeige und Anmeldung<br>"; break;
        case 'Teachers->list': $itemContent .= "Auflistung Kursleiter<br>"; break;
        case 'Courses->upload': $itemContent .= "Kurse Upload<br>"; break;
        case 'Courses->stats': $itemContent .= "Statistik Eingabe<br>"; break;
      }

      $itemContent .= "<div style='border-bottom: 1px solid #ccc; padding-top: 5px; margin-bottom: 5px;'></div>";

      $categs = $settings['settings.filterCategories'];

      if ($categs) {
        $cats = array();
        $db = $GLOBALS['TYPO3_DB'];
        $sql = "select C2.title as parent, C1.title as child from sys_category C1
                  inner join sys_category C2
                    on C1.parent = C2.uid
                  where C1.uid in ($categs)";

        $result = $db->sql_query($sql);
        while ($myrow = $db->sql_fetch_assoc($result)) {
          $cats[$myrow['parent']][] = $myrow['child'];
        }

        foreach($cats as $parent => $children) {
          $itemContent .= "$parent:<br>";
          foreach($children as $child) {
            $itemContent .= "- $child<br>";
          }
          $itemContent .= '<br>';
        }

      } else {
        $itemContent = "Gewählte Kategorien: Alle";
      }

    }

  }