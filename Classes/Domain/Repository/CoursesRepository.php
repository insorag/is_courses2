<?php
namespace INSOR\IsCourses2\Domain\Repository;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for Courses
 */
class CoursesRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * @var \TYPO3\CMS\Dbal\Database\DatabaseConnection
     */
    protected $db = null;

    private $basesql = null;

    public function initializeObject()
    {
        $this->db = $GLOBALS['TYPO3_DB'];

        $sql = "SHOW COLUMNS 
              FROM tx_iscourses2_domain_model_staticdata 
              WHERE Field not in (
                'uid','pid','categories','crdate','cruser_id','interner_bezeichner','l10n_diffsource','l10n_parent',
                'stammdatensatz_id','sys_language_uid','t3ver_count','t3ver_id','t3ver_label','t3ver_move_id','t3ver_oid',
                't3ver_stage','t3ver_state','t3ver_tstamp','t3ver_wsid','tstamp'
              )";

        $this->basesql = "SELECT
                        F.name as kursleiter, 
                        date_format(from_unixtime(C.start_datum), '%d.%m.%Y %H:%i') as start_datum_f,
                        date_format(from_unixtime(C.start_datum), '%d.%m.%Y') as start_datum_f_notime,
                        date_format(from_unixtime(C.end_datum), '%d.%m.%Y %H:%i') as end_datum_f,
                        C.*, ";

        foreach (\INSOR\IsCourses2\Helper\InsorLib::GetFirstColumn($this->db, $sql) as $overlaycol) {
            $this->basesql .= "COALESCE(NULLIF(S.$overlaycol, ''), C.$overlaycol) AS $overlaycol, ";
        }

        $this->basesql .= "FROM_UNIXTIME(start_datum, '%d.%m.%Y %H:%i') as start_datum_formatted";

        $this->basesql .= " FROM tx_iscourses2_domain_model_courses C
                          LEFT JOIN tx_iscourses2_domain_model_staticdata S
                            ON C.stammdatensatz_id = S.stammdatensatz_id
                          LEFT JOIN fe_users F
                            ON C.teacher = F.uid";

    }

    /**
     * Gibt alle Kurse als zweidimensionales Array zurück
     *
     * @return Array
     */
  public function findAll($categories = '', $limit = 0, $onlyBookable = false, $onlyFutureCourses = true,
                          $onlyFromThisFeuser = null, $onlyTheseCourseUids = null, $onlyYear = null, $includeHiddenCourses = null,
                          $categFilterAnd = false, $includeStatsRecords = false, $onlyFilledStats = false, $sys_language_uid = 0)
  {
        if ($categories) {
            if ($categFilterAnd) {
                $catgeg_count = count(explode(',', $categories));
                $category_where = "AND (
          (SELECT distinct count(*) 
              FROM sys_category_record_mm MM
              WHERE MM.uid_local in ($categories) 
              AND MM.uid_foreign = C.uid
              AND MM.tablenames = 'tx_iscourses2_domain_model_courses'
              AND MM.fieldname = 'categories'
            ) = $catgeg_count
          OR (
          (SELECT distinct count(*) 
              FROM sys_category_record_mm MM
              WHERE MM.uid_local in ($categories) 
              AND MM.uid_foreign = S.uid
              AND MM.tablenames = 'tx_iscourses2_domain_model_staticdata'
              AND MM.fieldname = 'categories'
            ) = $catgeg_count
          )
        )";
            } else {
                $category_where = "AND (
          C.uid IN (
            SELECT MM.uid_foreign 
              FROM sys_category_record_mm MM
              WHERE MM.uid_local in ($categories)
              AND MM.tablenames = 'tx_iscourses2_domain_model_courses'
              AND MM.fieldname = 'categories'
          )
          OR S.uid IN (
            SELECT MM.uid_foreign 
            FROM sys_category_record_mm MM
              WHERE MM.uid_local in ($categories)
              AND MM.tablenames = 'tx_iscourses2_domain_model_staticdata'
              AND MM.fieldname = 'categories'
          )
        )";
            }
        }

        if ($limit) {
            $limit_clause = "LIMIT 0, $limit";
        }

        if ($onlyBookable) {
            $bookable_clause = "AND C.anz_teilnehmer < C.max_teilnehmer";
        }

        if ($onlyFutureCourses) {
            $futurecourses_clause = "AND CASE WHEN COALESCE(NULLIF(S.anzeige_bis_enddatum, ''), C.anzeige_bis_enddatum) = 0 THEN start_datum ELSE end_datum END >= unix_timestamp(curdate())";
        }

        if ($onlyTheseCourseUids) {
            $courseuids_clause = "AND C.uid IN ($onlyTheseCourseUids)";
        }

        if ($onlyFromThisFeuser) {

            $users = $this->getFeuserChildren($onlyFromThisFeuser);
            $users[] = $onlyFromThisFeuser;
            $users = implode(',', $users);

            $onlyfeuser_clause = "AND C.owner IN ($users)";

        }

        if ($onlyYear) {
            $onlyyear_clause = "AND start_datum BETWEEN UNIX_TIMESTAMP('$onlyYear-01-01') AND UNIX_TIMESTAMP('$onlyYear-12-31')";
        }

        if (!$includeHiddenCourses) {
            $hidden_clause = 'AND C.hidden = 0';
        }

        if (!$includeStatsRecords) {
            $statsRecords_clause = 'AND C.stats_only_record = 0';
        }

        if ($onlyFilledStats) {
            $onlyFilledStats_clause = "AND C.statistik <> ''";
        }

    if ($sys_language_uid !== false) {
      $language_clause = "AND C.sys_language_uid = $sys_language_uid";
    }

        // TODO: Hidden Ansicht parametrisierbar machen
        $sql = "{$this->basesql}
                WHERE C.deleted = 0 
                $hidden_clause 
                $futurecourses_clause
                $category_where
                $bookable_clause
                $courseuids_clause
                $onlyfeuser_clause
                $onlyyear_clause
                $statsRecords_clause
                $onlyFilledStats_clause
                $language_clause
                ORDER BY start_datum ASC
                $limit_clause";
        $dataset = \INSOR\IsCourses2\Helper\InsorLib::GetDataset($this->db, $sql);

        if (count($dataset) > 0) {

            // Anreichern des Datasets um Kategorien ID. Hierfür erst mal alle Kurs
            // UIDs holen, damit wir die Kategorien Abfrage ein wenig einschränken
            // können.
            foreach ($dataset as $uid => $data) {
                $course_uids[] = $uid;
            }
            $course_uids = implode(',', $course_uids);

            // Alle Kategorien für diese Kurse holen
            $sql = "SELECT uid_foreign, uid_local, 'C' as orderkey 
                  FROM sys_category_record_mm MM
                    WHERE MM.tablenames = 'tx_iscourses2_domain_model_courses' 
                    AND MM.fieldname = 'categories'
                    AND uid_foreign IN ($course_uids)
                UNION ALL
                SELECT C.uid as uid_foreign, uid_local, 'S' as orderkey 
                  FROM sys_category_record_mm MM
                    INNER JOIN tx_iscourses2_domain_model_staticdata S
                    ON MM.uid_foreign = S.uid
                  INNER JOIN tx_iscourses2_domain_model_courses C
                    ON S.stammdatensatz_id = C.stammdatensatz_id
                    WHERE MM.tablenames = 'tx_iscourses2_domain_model_staticdata' 
                    AND MM.fieldname = 'categories'
                    AND C.uid IN ($course_uids)
                ORDER BY 3";

      $records = \INSOR\IsCourses2\Helper\InsorLib::GetDataset($this->db, $sql) ?: array();
      if ($records) {
        foreach ($records as $record) {
          $courseCategories[$record['uid_foreign']][] = $record['uid_local'];
        }
      }

      if ($courseCategories) {
        foreach ($dataset as &$row) {
          $row['categories'] = implode(',', $courseCategories[$row['uid']]);
        }
            }

            foreach ($dataset as &$row) {
                if ($row['picture']) {
                    unset($row['picture']);
                    $row['picture'] = $this->getPicturesOfCourse($row['uid']);
                }
            }

            return $dataset;
        } else {
            return array();
        }
    }

    /**
     * Rekursive Funktion zum Auslesen aller FE-User Children (Downline).
     * Anwendungsfall: Koordinationsstelle loggt sich ein, und es sollen alle User zurückgegeben werden,
     * die im Baum unterhalb der Koordinationsstelle hängen.
     *
     * @param $fe_user_id
     * @param null $list
     * @return array|null
     */
  private function getFeuserChildren($fe_user_id, $list = null) {

    if (!$list) $list = array();

        $sql = "SELECT uid FROM fe_users WHERE parent = $fe_user_id AND uid <> parent";
        $children = \INSOR\IsCourses2\Helper\InsorLib::GetFirstColumn($this->db, $sql);

        if (is_array($children) && (sizeof($children) > 0)) {
            foreach ($children as $element) {
                $list[] = $element;
                $list = $this->getFeuserChildren($element, $list);
            }
        }

        return $list;
    }

    /**
     * Rekursive Funktion zum Auslesen der FE-User Parents (Rootline).
     *
     * @param $fe_user_id
     * @param $list
     * @return array|null
     */
  public function getFeuserRootline($fe_user_id, $list = null) {
    if (!$list) $list = array($fe_user_id);

        $sql = "SELECT parent FROM fe_users WHERE uid = $fe_user_id";
        $parent = \INSOR\IsCourses2\Helper\InsorLib::GetFirstColumn($this->db, $sql);

        if (is_array($parent)) {
            foreach ($parent as $element) {
                $list[] = $element;
                $list = $this->getFeuserRootline($element, $list);
            }
        }

        return $list;
    }

    /**
     * @param $course_uid
     * @param $fe_user_ids
     * @return mixed
     */
  public function getConfiguredStatsForm($course_uid, $fe_user_ids) {
        $fe_user_ids[] = 0;
        $fe_user_ids = implode(',', $fe_user_ids);

        $sql = "select powermail_form_id 
              from sys_category_record_mm M
              inner join tx_iscourses2_domain_model_statistikfragebogenlink L
                ON M.uid_local = L.category_id
              where uid_foreign = $course_uid
              and fe_admin_id in ($fe_user_ids)
              ORDER BY crdate DESC
              LIMIT 0,1";
        return \INSOR\IsCourses2\Helper\InsorLib::GetFirstField($this->db, $sql);
    }

    /**
     * Gibt alle Bilder zu einem Kurs zurück
     *
     * @param $uid
     * @return array
     */
  public function getPicturesOfCourse($uid) {
        $sql = "select R.uid as reference_uid, F.uid, F.identifier, F.name, F.extension 
                      from sys_file F
                      inner join sys_file_reference R
                        ON R.uid_local = F.uid
                        AND R.deleted = 0
                      where R.tablenames = 'tx_iscourses2_domain_model_courses'
                      and R.fieldname = 'picture'
                      AND R.uid_foreign = $uid";
        foreach (\INSOR\IsCourses2\Helper\InsorLib::GetDataset($this->db, $sql) as $picture) {
            $pics[] = $picture;
        }
        return $pics;
    }

    /**
     * Git einen bestimmten Kurs als Array zurück
     *
     * @param integer UID des Kurses
     * @return Array
     */
  public function findByUid($uid) {
        $sql = "{$this->basesql}
                WHERE C.deleted = 0 
                /*AND C.hidden = 0*/
                AND C.uid = $uid
                ORDER BY start_datum ASC";
        return \INSOR\IsCourses2\Helper\InsorLib::GetFirstRow($this->db, $sql);
    }

    /**
     * Gibt Kategorien einer bestimmten Parent Kategorie zurück
     *
     * @param mixed $parent
     */
  public function findCategories($parent) {
        $sql = "SELECT uid, 
                (SELECT title 
                    FROM sys_category C2 
                    WHERE C2.uid = C1.uid
                    AND C2.hidden = 0
                    AND C2.deleted = 0
                    OR (l10n_parent = C1.uid AND sys_language_uid = {$GLOBALS['TSFE']->sys_language_uid})
                    ORDER BY sys_language_uid desc limit 0,1
                ) as title 
                FROM sys_category C1
                WHERE parent = $parent
                AND sys_language_uid = 0
                AND deleted = 0
                AND hidden = 0
                ORDER BY 2, 1";
        return \INSOR\IsCourses2\Helper\InsorLib::GetDataset($this->db, $sql);
    }

    /**
     * Gibt Parent-Kategorie einer bestimmten Kategorie zurück
     *
     * @param int $category_uid
     * @return string
     */
  public function findParentCategory($category_uid) {
        $sql = "SELECT parent
                FROM sys_category
                WHERE uid = $category_uid";
        return \INSOR\IsCourses2\Helper\InsorLib::GetFirstField($this->db, $sql);
    }

    /**
     * Gibt die zugehörigen Kategorien eines Kurses zurück
     *
     * @param $course_uid
     * @param null $parent
     * @return array
     */
  public function findCourseCategories($course_uid, $parent = null) {
    if ($parent) $parent_where = "AND C1.parent = $parent";
        $sql = "SELECT C1.uid, (SELECT title 
                    FROM sys_category C2 
                    WHERE C2.uid = C1.uid 
                    AND C2.hidden = 0
                    AND C2.deleted = 0
                    OR (l10n_parent = C1.uid AND sys_language_uid = {$GLOBALS['TSFE']->sys_language_uid})
                    ORDER BY C2.sys_language_uid desc limit 0,1
                ) as title
              FROM sys_category_record_mm MM
              INNER JOIN sys_category C1
                ON MM.uid_local = C1.uid
                AND MM.uid_foreign = $course_uid
                AND MM.tablenames = 'tx_iscourses2_domain_model_courses'
                AND MM.fieldname = 'categories'
                $parent_where
                AND C1.sys_language_uid = 0
                AND hidden = 0
                AND deleted = 0";
        return \INSOR\IsCourses2\Helper\InsorLib::GetDataset($this->db, $sql) ?: array();
    }

    /**
     * Holt alle Lehrer, welche einem Kurs zugeordnet sind
     *
     * @param $course_uid
     * @return array
     */
  public function findCourseTeachers($course_uid) {
        $sql = "SELECT F.* 
              FROM tx_iscourses2_courses_teachers_mm MM
              INNER JOIN fe_users F
                ON MM.uid_foreign = F.uid
                AND MM.uid_local = $course_uid
              ORDER BY F.name";
        return \INSOR\IsCourses2\Helper\InsorLib::GetDataset($this->db, $sql) ?: array();
    }

    /**
     * @param $course_uid
     * @param $args
     * @return int
     */
  public function updateCourse($course_uid, $args) {
    if ($args['start_datum']) { $args['start_datum'] = strtotime($args['start_datum']); }
    if ($args['end_datum']) { $args['end_datum'] = strtotime($args['end_datum']); }
    if ($args['anmeldeschluss_datum']) { $args['anmeldeschluss_datum'] = strtotime($args['anmeldeschluss_datum']); }

        return \INSOR\IsCourses2\Helper\InsorLib::InsertOrUpdate(
          $this->db,
          'tx_iscourses2_domain_model_courses',
          $args,
          $course_uid,
          true
        );
    }

    /**
     * @param $course_uid
     * @param $categories
     */
    public function setNewCourseCategories($course_uid, $categories)
    {
        // Alle Kategorien der entsprechenden Parents löschen
        if (is_array($categories) || is_object($categories)) {
          $parents = implode(',', array_keys($categories));
        }
        $this->db->sql_query("DELETE MM 
                            FROM sys_category C
                            INNER JOIN sys_category_record_mm MM
                              ON C.uid = MM.uid_local
                              AND MM.tablenames = 'tx_iscourses2_domain_model_courses'
                              AND MM.fieldname = 'categories'
                            WHERE MM.uid_foreign = $course_uid
                              AND C.parent IN ($parents)");

        // Neue Kategorien Referenzen anlegen
        if (is_array($categories) || is_object($categories)) {
          foreach ($categories as $parent => $categories) {
              foreach ($categories as $category_uid) {
                  \INSOR\IsCourses2\Helper\InsorLib::InsertOrUpdate($this->db, 'sys_category_record_mm', array(
                    'uid_local' => $category_uid,
                    'uid_foreign' => $course_uid,
                    'tablenames' => 'tx_iscourses2_domain_model_courses',
                    'fieldname' => 'categories'
                  ), null);
              }
          }
        }
    }

    /**
     * Gibt alle Kurse zurück, welche in Verbindung mit dem User stehen
     *
     * @param $feuser_uid
     * @return array
     */
  public function getCoursesByTeacher($feuser_uid)
  {
        if (is_array($feuser_uid)) {
            $feuser_uid = implode(',', $feuser_uid);
        }
        $sql = "SELECT uid_local FROM tx_iscourses2_courses_teachers_mm WHERE uid_foreign IN ($feuser_uid)";
        return \INSOR\IsCourses2\Helper\InsorLib::GetFirstColumn($this->db, $sql);
    }

    /**
     * Gibt alle Kurse zurück, welche in Verbindung mit dem User oder seiner Downline stehen
     *
     * @param $feuser_uid
     * @return array
     */
  public function getCoursesByOwner($feuser_uid)
  {

    $users = $this->getFeuserChildren($feuser_uid);

        $courses_1 = $this->getCoursesByTeacher($users);

        // Deprecated, Owner wird künftig nicht mehr verwendet, wir arbeiten mit FE User Chaining.
        $sql = "SELECT uid FROM tx_iscourses2_domain_model_courses WHERE owner = $feuser_uid";
        $courses_2 = \INSOR\IsCourses2\Helper\InsorLib::GetFirstColumn($this->db, $sql);

        $result = array();
    if (is_array($courses_1)) $result = array_merge($result, $courses_1);
    if (is_array($courses_2)) $result = array_merge($result, $courses_2);

        return $result;
    }


    /**
     * Fügt mehrere FE User einem Kurs an. Zuvor werden alle anderen Relationen
     * gelöscht!
     *
     * @param $feuser_uids
     * @param $course_uid
     * @return int
     */
  public function replaceFeusersToCourse($course_uid, $feuser_uids)
  {
        // Zuerst alle bestehenden Zuweisungen löschen
        $this->db->sql_query("DELETE FROM tx_iscourses2_courses_teachers_mm 
                            WHERE uid_local = $course_uid");

        // Nun die User in die Relation Tabelle eintragen
        foreach ($feuser_uids as $feuser_uid) {
            \INSOR\IsCourses2\Helper\InsorLib::InsertOrUpdate(
              $this->db,
              'tx_iscourses2_courses_teachers_mm',
              array(
                'uid_local' => $course_uid,
                'uid_foreign' => $feuser_uid
              ),
              null,
              false
            );
        }

        \INSOR\IsCourses2\Helper\InsorLib::InsertOrUpdate(
          $this->db,
          'tx_iscourses2_domain_model_courses',
          array(
            'teacher' => count($feuser_uids),
          ),
          $course_uid,
          false
        );
    }

  public function removeFileReference($identifier) {
        $sql = "DELETE FROM sys_file WHERE identifier like '%/tx_iscourses2/$identifier'";
        $this->db->sql_query($sql);
    }

    /**
     * Findet ähnliche Kurse wie den im Parameter angegebenen.
     *
     * Sucht sich ähnliche Kurse in der Datenbank, und gibt solche zurückt, wenn gefunden. Dabei werden nur jene Kurse
     * zurück gegeben, welche den Mindest-Similarity-Score (0-100) überschreiten.
     *
     * @param $course
     * @param $min_score
     * @return mixed
     */
  public function findSimilar($course, $min_score) {
        $similars = array();

        $start_datum = strtotime($course['start_datum']);
        $titel = addslashes($course['titel']);

        $sql = "SELECT * 
              FROM tx_iscourses2_domain_model_courses
              WHERE titel like '%$titel%'
              AND start_datum = '$start_datum'
              AND deleted = 0"; //die($sql);
        $dataset = \INSOR\IsCourses2\Helper\InsorLib::GetDataset($this->db, $sql);

        foreach ($dataset as $line) {
            $score = 50;
            $similar_fields = array(0 => 'titel');
            // TODO Felder die getestet werden mit Score an Funktion uebergeben, damit dynamisch (muss via typoscript in controller definiert werden)
      $tested_fields = ['kurstage','kursort','anmelde_url','kosten','owner','kontaktperson','teacher_text'];

            // Gleiche Kurstage? Starkes Signal für mögliches Duplikat
            if ($course['kurstage'] == $line['kurstage']) {
                $score += 15;
                $similar_fields[] = 'kurstage';
            }

            // Gleicher Kursort zwischen zwei verschiedenen Durchführungen wäre möglich, ist aber ein Indiz
            if ($course['kursort'] == $line['kursort']) {
                $score += 10;
                $similar_fields[] = 'kursort';
            }

            // Gleiche Anmelde-URL? Kann auch bei verschiedenen Durchführungen sein, trotzdem ein Indiz
            if ($course['anmelde_url'] == $line['anmelde_url']) {
                $score += 10;
                $similar_fields[] = 'anmelde_url';
            }

            // Kosten sind oft für verschiedene Durchführungen die gleichen, daher ein eher schwaches Indiz
            if ($course['kosten'] == $line['kosten']) {
                $score += 5;
                $similar_fields[] = 'kosten';
            }

            // Uploadende Person die gleiche wie andere Durchführung, kann auch sein, aber auch schwaches Indiz
            if ($course['owner'] == $line['owner']) {
                $score += 5;
                $similar_fields[] = 'owner';
                $same_owner = 1;
            } else {
                $same_owner = 0;
            }

            // Kontaktperson Sekretariat ist gleiches, das kann gut möglich sein, schwaches Indiz
            if ($course['kontaktperson'] == $line['kontaktperson']) {
                $score += 5;
                $similar_fields[] = 'kontaktperson';
            }

            // Lehrer ist derselbe wie andere Durchführung, kann gut sein, ebenfalls schwaches Indiz
            if ($course['teacher_text'] == $line['teacher_text']) {
                $score += 5;
                $similar_fields[] = 'teacher_text';
            }

            if ($score >= $min_score) {
                $similars[$line['uid']] = $line;
                $similars[$line['uid']]['similarity_score'] = min($score, 100);
                $similars[$line['uid']]['similar_fields'] = $similar_fields;
                $similars[$line['uid']]['same_owner'] = $same_owner;
            }
        }

        return $similars;
    }

    /**
     * @param $uid Kurs-UID falls fuer bestimmten Kurs
     * @return array
     */
  public function findStatMailCourses($uid = 0) {
        $sql = "
      SELECT DISTINCT 
        C.uid as course_uid,
        C.titel as course_titel,
        C.end_datum as course_end_datum,
        C.stats_reminder_sent as course_stats_reminder_sent,
        C.hidden as course_hidden,
        C.deleted as course_deleted,
        C.teacher_email as course_teacher_email,
        C.teacher_text as course_teacher_text,
        C.owner as owner,
        C.statistik as statistik,
        (SELECT uid_foreign
          FROM tx_iscourses2_courses_teachers_mm
          WHERE uid_local = C.uid
          ORDER BY sorting ASC, uid_foreign DESC
          LIMIT 0,1
        ) as fe_user_id
      FROM tx_iscourses2_domain_model_courses as C
      WHERE
        C.end_datum+86400 < UNIX_TIMESTAMP(now())
        AND C.end_datum > 1514764800
        AND C.statistik = ''
        AND C.stats_only_record = 0
        AND C.hidden = 0
        AND C.deleted = 0
    ";
        if ($uid != 0) {
            $sql .= " AND C.uid = $uid ";
        } else {
            $sql .= "
        AND C.stats_reminder_sent = 0
      ";
        }

        $dataset = \INSOR\IsCourses2\Helper\InsorLib::GetDataset($this->db, $sql);
        return $dataset;
    }

    /**
     * Schreibt uebergebenen Text in die "Log" spalte und setzt einen Newline hinten an.
     *
     * @param $text string Text der in die Spalte "Log" geschrieben wird
     * @param $course_uid integer UID vom Kurs
     * @return null
     */
  public function writeToCourseLog($text,$course_uid) {
        // Urspruengliche Log Daten holen vom Kurs
        $log_sql = "
      SELECT log
        FROM tx_iscourses2_domain_model_courses
      WHERE
        uid = $course_uid
    ";
        // Log String direkt in variable speichern
        $log = \INSOR\IsCourses2\Helper\InsorLib::GetFirstField($this->db, $log_sql);

        $date = date("Y-m-d H:i:s");

        // Log String schreiben
        \INSOR\IsCourses2\Helper\InsorLib::InsertOrUpdate(
          $this->db,
          'tx_iscourses2_domain_model_courses',
          [
            'log' => $log . $date . ": " . $text . "\n"
          ],
          $course_uid
        );

        return true;
    }

    /**
     * Schreibt Stat Reminder Datum (timestamp) in Kurs
     *
     * @param $course_uid integer UID vom Kurs
     * @return null
     */
  public function writeStatsReminderSent($course_uid) {
        // Stat Reminder Datum schreiben
        \INSOR\IsCourses2\Helper\InsorLib::InsertOrUpdate(
          $this->db,
          'tx_iscourses2_domain_model_courses',
          [
            'stats_reminder_sent' => time()
          ],
          $course_uid
        );

        return true;
    }

    /**
     * Loescht ein Kursbild aus der Datenbank
     *
     * @param $course_uid
     * @return bool
     */
    public function deleteCoursePic($course_uid)
    {
        // Stat Reminder Datum schreiben
        $sql = "DELETE FROM sys_file_processedfile WHERE identifier like '%coursepic.".$course_uid."%'";
        $this->db->sql_query($sql);

        // Stat Reminder Datum schreiben
        $sql = "DELETE FROM sys_file WHERE identifier like '%coursepic.".$course_uid."%'";
        $this->db->sql_query($sql);

        return true;
    }


}