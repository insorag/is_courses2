/**
 *
 * @param $element im javascript context this
 */
function mailReminder ($element) {
  var course_uid = $($element).parent().parent().data('uid');
  var course_name = $($element).parent().parent().find('.titel a').text();
  var course_referent = $($element).parent().parent().find('.referent .teacher_text').text();
  var confirm_message = confirm(window.statmail_lang["confirm"] +
    "\n\nID: " + course_uid + "\n" + window.statmail_lang["name"] + course_name );

  if(confirm_message === true) {
    var url = window.location.pathname +
      '/?tx_iscourses2_courses[action]=mailStats&tx_iscourses2_courses[controller]=Courses&type=1200';
    $.ajax({
      url: url,
      data: 'tx_iscourses2_courses[course_uid]='+course_uid,
      type: "GET",
      dataType: 'text'
    }).done(function(data){
      if(data == '1') {
        alert(window.statmail_lang["reminder_success_pre"] + '"' + course_name + '"' + window.statmail_lang["reminder_success_after"]);
      } else if(data == '0') {
        alert(window.statmail_lang["reminder_keinreferent_pre"] + '"' + course_name + '"' + window.statmail_lang["reminder_keinreferent_after"]);
      }
      else if(data == '00') {
        alert(window.statmail_lang["reminder_referentungueltig_pre"] + '"' + course_name + '"' + window.statmail_lang["reminder_referentungueltig_after"]);
      }
      else if(data == '000') {
        alert(window.statmail_lang["reminder_mailerror_pre"] + '"' + course_name + '"' + window.statmail_lang["reminder_mailerror_after"]);
      }
    });
  }
}