<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'INSOR.' . $_EXTKEY,
	'Courses',
	array(
		'Courses' => 'list, edit, update, upload, show, stats, mailStats',
		'Teachers' => 'list, edit, update, show',
		
	),
	// non-cacheable actions
	array(
		'Courses' => 'edit, upload, stats, mailStats',
		'Teachers' => 'edit, update',
		
	)
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['INSOR\\IsCourses2\\Task\\StatsMailTask'] = array(
  'extension'        => $_EXTKEY,
  'title'            => 'Statistik-Mail Scheduler Task',
  'description'      => 'Erstellt täglich für beendete Kurse ein Email für den zugewiesenen Referenten, in welchem er zur Statistik-Erfassung aufgerufen wird.'
);

//$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem'][$_EXTKEY] = 'EXT:your_ext/Classes/Hooks/PageLayoutView.php:INSOR\IsCourses2\Hooks\PageLayoutView';

// enable Powermail SignalSlot
  $signalSlotDispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Extbase\SignalSlot\Dispatcher');
  $signalSlotDispatcher->connect(
    'In2code\Powermail\Controller\FormController',
    'createActionBeforeRenderView',
    'Insor\IsCourses2\Controller\FormController',
    'manipulateMailObjectOnCreate',
    FALSE
  );