<?php
namespace INSOR\IsCourses2\Domain\Repository;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * The repository for Teachers
 */
class TeachersRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
  /**
   * @var \TYPO3\CMS\Dbal\Database\DatabaseConnection
   */
  protected $db = null;

  public function initializeObject()
  {
      $this->db = $GLOBALS['TYPO3_DB'];
  }

  /**
   * Find fe_user by FE User-ID
   *
   * @param integer $uid
   * @param bool $profileActive
   * @return mixed|object
   */
  public function findByUid($uid, $profileActive = false) {
    $sql = "SELECT * from fe_users WHERE uid = $uid AND deleted = 0";
    $sql .= ($profileActive) ? " AND online_profil_aktiv = 1" : "";
    return \INSOR\IsCourses2\Helper\InsorLib::GetFirstRow($this->db, $sql);
  }

  /**
   * Find fe_user bei FE Name
   *
   * @param $name
   * @param $profileActive
   * @return mixed
   */
  public function findByName($name,$profileActive = false) {
    $name_reversed = implode(' ', array_reverse(explode(' ', $name)));
    $sql = "SELECT * from fe_users 
              WHERE (name = '$name' OR name = '$name_reversed') 
              AND deleted = 0";
    $sql .= ($profileActive) ? " AND online_profil_aktiv=1" : "";
    return \INSOR\IsCourses2\Helper\InsorLib::GetFirstRow($this->db, $sql);
  }

  /**
   * Find fe_user bei FE Username
   *
   * @param $name
   * @param bool $profileActive
   * @return mixed
   */
  public function findByUsername($name, $profileActive = false) {
    $sql = "SELECT * from fe_users 
              WHERE (username = '$name') 
              AND deleted = 0";
    $sql .= ($profileActive) ? " AND online_profil_aktiv=1" : "";
    return \INSOR\IsCourses2\Helper\InsorLib::GetFirstRow($this->db, $sql);
  }

  /**
   * Aktualisiert einen FE User Datensatz
   * @param $feuser_uid
   * @param $args
   * @return int
   */
  public function updateFeuser($feuser_uid, $args)
  {
    return \INSOR\IsCourses2\Helper\InsorLib::InsertOrUpdate($this->db, 'fe_users', $args, $feuser_uid, true);
  }

  /**
   * Gibt ein Array mit allen Kategorien UIDs eines FE-Users zurück
   * @param $feuser_uid
   * @param null $parent
   * @return array
   */
  public function getFeuserCategories($feuser_uid, $parent = null):array {
    if ($parent) $parent_where = "AND C1.parent = $parent";
    $sql = "SELECT C1.uid, (SELECT title 
                    FROM sys_category C2 
                    WHERE C2.uid = C1.uid 
                    OR (l10n_parent = C1.uid AND sys_language_uid = {$GLOBALS['TSFE']->sys_language_uid})
                    ORDER BY C2.sys_language_uid desc limit 0,1
                ) as title
              FROM sys_category_record_mm MM
              INNER JOIN sys_category C1
                ON MM.uid_local = C1.uid
                AND MM.uid_foreign = $feuser_uid
                AND MM.tablenames = 'fe_users'
                AND MM.fieldname = 'categories'
                $parent_where";
    return \INSOR\IsCourses2\Helper\InsorLib::GetDataset($this->db, $sql) ?: array();
  }

  /**
   * Setzt für den FE-User entsprechende neue Kategorien
   * @param $feuser_uid
   * @param $categories
   */
  public function setNewFeuserCategories($feuser_uid, $categories)
  {
    // Alle Kategorien der entsprechenden Parents löschen
    $parents = implode(',', array_keys($categories));
    $this->db->sql_query("DELETE MM 
                            FROM sys_category C
                            INNER JOIN sys_category_record_mm MM
                              ON C.uid = MM.uid_local
                              AND MM.tablenames = 'fe_users'
                              AND MM.fieldname = 'categories'
                            WHERE MM.uid_foreign = $feuser_uid
                              AND C.parent IN ($parents)");

    // Neue Kategorien Referenzen anlegen
    foreach($categories as $parent => $categories) {
      foreach ($categories as $category_uid) {
        \INSOR\IsCourses2\Helper\InsorLib::InsertOrUpdate($this->db, 'sys_category_record_mm', array(
          'uid_local' => $category_uid,
          'uid_foreign' => $feuser_uid,
          'tablenames' => 'fe_users',
          'fieldname' => 'categories'
        ), null);
      }
    }
  }

  /**
   * Gibt alle Dozenten zurück
   * @return array
   */
  public function findAll($categories = '') {

    if ($categories) {
      $category_where = "AND uid IN (
        SELECT MM.uid_foreign 
          FROM sys_category_record_mm MM
          WHERE MM.uid_local in ($categories)
          AND MM.tablenames = 'fe_users'
          AND MM.fieldname = 'categories'
        )";
    }

    $sql = "SELECT * FROM fe_users 
              WHERE deleted = 0 
              AND disable = 0 
              AND tx_extbase_type = 'Tx_IsCourses2_Teachers'
              AND online_profil_aktiv = 1
              $category_where
              ORDER BY name, last_name, first_name ASC";
    return \INSOR\IsCourses2\Helper\InsorLib::GetDataset($this->db, $sql);
  }

  public function removeFileReference($identifier) {
    $sql = "DELETE FROM sys_file WHERE identifier = '/tx_iscourses2/$identifier'";
    $this->db->sql_query($sql);
  }

}