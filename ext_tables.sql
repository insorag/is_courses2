#
# Table structure for table 'tx_iscourses2_domain_model_courses'
#
CREATE TABLE tx_iscourses2_domain_model_courses (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	titel varchar(255) DEFAULT '' NOT NULL,
	beschreibung text NOT NULL,
	kosten varchar(255) DEFAULT '' NOT NULL,
	dauer varchar(255) DEFAULT '' NOT NULL,
	kurstage text NOT NULL,
	detail_url varchar(255) DEFAULT '' NOT NULL,
	anmelde_url varchar(255) DEFAULT '' NOT NULL,
	kurs_adresse text NOT NULL,
	kursort varchar(255) DEFAULT '' NOT NULL,
	start_datum int(11) DEFAULT '0' NOT NULL,
	end_datum int(11) DEFAULT '0' NOT NULL,
	anmeldeschluss_datum int(11) DEFAULT '0' NOT NULL,
	anz_teilnehmer int(11) DEFAULT '0' NOT NULL,
	max_teilnehmer int(11) DEFAULT '0' NOT NULL,
	status int(11) DEFAULT '0' NOT NULL,
	status_text varchar(255) DEFAULT '' NOT NULL,
	external_id varchar(255) DEFAULT '' NOT NULL,
	stammdatensatz_id varchar(255) DEFAULT '' NOT NULL,
	anzeige_bis_enddatum tinyint(1) unsigned DEFAULT '0' NOT NULL,
	unterlagen_links text NOT NULL,
	teacher_text varchar(255) DEFAULT '' NOT NULL,
	picture int(11) unsigned NOT NULL default '0',
	statistik text NOT NULL,
	log text NOT NULL,
	stats_only_record tinyint(1) unsigned DEFAULT '0' NOT NULL,
	stats_reminder_sent int(11) DEFAULT '0' NOT NULL,
	teacher int(11) unsigned DEFAULT '0' NOT NULL,
	owner int(11) unsigned DEFAULT '0',

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,
	sorting int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'fe_users'
#
CREATE TABLE fe_users (

	external_id varchar(255) DEFAULT '' NOT NULL,
	themen text NOT NULL,
	werdegang text NOT NULL,
	ist_mitglied tinyint(1) unsigned DEFAULT '0' NOT NULL,
	online_profil_aktiv tinyint(1) unsigned DEFAULT '0' NOT NULL,
	bemerkungen text NOT NULL,
	keine_benachrichtigungen tinyint(1) unsigned DEFAULT '0' NOT NULL,
	parent int(11) unsigned DEFAULT '0',

	tx_extbase_type varchar(255) DEFAULT '' NOT NULL,

);

#
# Table structure for table 'tx_iscourses2_domain_model_staticdata'
#
CREATE TABLE tx_iscourses2_domain_model_staticdata (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	interner_bezeichner varchar(255) DEFAULT '' NOT NULL,
	stammdatensatz_id varchar(255) DEFAULT '' NOT NULL,
	detail_url varchar(255) DEFAULT '' NOT NULL,
	anmelde_url varchar(255) DEFAULT '' NOT NULL,
	kosten text NOT NULL,
	unterlagen_links text NOT NULL,
	anzeige_bis_enddatum tinyint(1) unsigned DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_iscourses2_domain_model_statistikfragebogenlink'
#
CREATE TABLE tx_iscourses2_domain_model_statistikfragebogenlink (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	powermail_form_id int(11) DEFAULT '0' NOT NULL,
	category_id int(11) DEFAULT '0' NOT NULL,
	fe_admin_id int(11) unsigned DEFAULT '0',

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
 KEY language (l10n_parent,sys_language_uid)

);

#
# Table structure for table 'tx_iscourses2_domain_model_courses'
#
CREATE TABLE tx_iscourses2_domain_model_courses (
	categories int(11) unsigned DEFAULT '0' NOT NULL,
);

#
# Table structure for table 'tx_iscourses2_courses_teachers_mm'
#
CREATE TABLE tx_iscourses2_courses_teachers_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'fe_users'
#
CREATE TABLE fe_users (
	categories int(11) unsigned DEFAULT '0' NOT NULL,
);

#
# Table structure for table 'tx_iscourses2_domain_model_staticdata'
#
CREATE TABLE tx_iscourses2_domain_model_staticdata (
	categories int(11) unsigned DEFAULT '0' NOT NULL,
);
