<?php
  namespace Insor\IsCourses2\Controller;

  /**
   * Implementation Powermail Signal/Slot für die Speicherung der Powermail-Statistik-Formularwerte
   * in den entsprechenden Datensatz des Kurses
   */
  class FormController {

    /**
     * @var \TYPO3\CMS\Dbal\Database\DatabaseConnection
     */
    protected $db = null;

    /**
     * @param \In2code\Powermail\Domain\Model\Mail $mail
     * @param string $hash
     * @param \In2code\Powermail\Controller\FormController $pObj
     */
    public function manipulateMailObjectOnCreate($mail, $hash, $pObj) {
      /** @var \In2code\Powermail\Domain\Model\Answer */
      $answer = null;

      /** @var \In2code\Powermail\Domain\Model\Field */
      $field = null;

      // Alle Antworten in ein Array speichern
      foreach ($mail->getAnswers() as $answer) {
        $field = $answer->getField();
        $content[$field->getMarker()] = $answer->getValue();
      }

      // Datenbank initialisieren
      // Nachschauen, ob die confirmationRedirection in den Settings von is_courses aktiviert ist. Falls ja,
      // lesen wir das konfigurierte Feld fieldnameWithTargetPid aus, und leiten auf diese Page dann um. Dabei
      // übergeben wir alle Parameter ans Zielformular
      $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
      $configurationManager = $objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManagerInterface');
      $settings = $configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);
      $settings = $settings['plugin.']['tx_iscourses2_courses.']['settings.'];
      if ($settings['confirmationRedirection.']['enable'] == '1') {
        $targetPidField = $settings['confirmationRedirection.']['fieldnameWithTargetPid'];
        $targetPid = $content[$targetPidField];
        $parameterString = http_build_query($content);
        $url = "/index.php?id=$targetPid&$parameterString";
        header("Location: $url", 302);
      }
    }

  }