<?php
return array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_courses',
		'label' => 'titel',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,
		'sortby' => 'sorting',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'titel,beschreibung,kosten,dauer,kurstage,detail_url,anmelde_url,kurs_adresse,kursort,start_datum,end_datum,anmeldeschluss_datum,anz_teilnehmer,max_teilnehmer,status,external_id,stammdatensatz_id,anzeige_bis_enddatum,unterlagen_links,teacher_text,picture,statistik,log,stats_only_record,stats_reminder_sent,teacher,owner,',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('is_courses2') . 'Resources/Public/Icons/tx_iscourses2_domain_model_courses.gif'
	),
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, titel, beschreibung, kosten, dauer, kurstage, detail_url, anmelde_url, kurs_adresse, kursort, start_datum, end_datum, anmeldeschluss_datum, anz_teilnehmer, max_teilnehmer, status, external_id, stammdatensatz_id, anzeige_bis_enddatum, unterlagen_links, teacher_text, picture, statistik, log, stats_only_record, stats_reminder_sent, teacher, owner',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, titel, beschreibung, kosten, dauer, kurstage, detail_url, anmelde_url, kurs_adresse, kursort, start_datum, end_datum, anmeldeschluss_datum, anz_teilnehmer, max_teilnehmer, status, external_id, stammdatensatz_id, anzeige_bis_enddatum, unterlagen_links, teacher_text, picture, statistik, log, stats_only_record, stats_reminder_sent, teacher, owner, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
	
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_iscourses2_domain_model_courses',
				'foreign_table_where' => 'AND tx_iscourses2_domain_model_courses.pid=###CURRENT_PID### AND tx_iscourses2_domain_model_courses.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
	
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),

		'titel' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_courses.titel',
			'config' => array(
				'type' => 'text',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'beschreibung' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_courses.beschreibung',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			)
		),
		'kosten' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_courses.kosten',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'dauer' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_courses.dauer',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'kurstage' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_courses.kurstage',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			)
		),
		'detail_url' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_courses.detail_url',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'anmelde_url' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_courses.anmelde_url',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'kurs_adresse' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_courses.kurs_adresse',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			)
		),
		'kursort' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_courses.kursort',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'start_datum' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_courses.start_datum',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'eval' => 'datetime,required',
				'checkbox' => 1,
				'default' => time()
			),
		),
		'end_datum' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_courses.end_datum',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'eval' => 'datetime',
				'checkbox' => 1,
				'default' => time()
			),
		),
		'anmeldeschluss_datum' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_courses.anmeldeschluss_datum',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'eval' => 'datetime',
				'checkbox' => 1,
				'default' => time()
			),
		),
		'anz_teilnehmer' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_courses.anz_teilnehmer',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			)
		),
		'max_teilnehmer' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_courses.max_teilnehmer',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			)
		),
		'status' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_courses.status',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => array(
					array('-- Label --', 0),
				),
				'size' => 1,
				'maxitems' => 1,
				'eval' => ''
			),
		),
		'external_id' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_courses.external_id',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'stammdatensatz_id' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_courses.stammdatensatz_id',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'anzeige_bis_enddatum' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_courses.anzeige_bis_enddatum',
			'config' => array(
				'type' => 'check',
				'default' => 0
			)
		),
		'unterlagen_links' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_courses.unterlagen_links',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			)
		),
		'teacher_text' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_courses.teacher_text',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'picture' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_courses.picture',
			'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
				'picture',
				array(
					'appearance' => array(
						'createNewRelationLinkTitle' => 'LLL:EXT:cms/locallang_ttc.xlf:images.addFileReference'
					),
					'foreign_types' => array(
						'0' => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						),
						\TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => array(
							'showitem' => '
							--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
						)
					),
					'maxitems' => 1
				),
				$GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
			),
		),
		'statistik' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_courses.statistik',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			)
		),
		'log' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_courses.log',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			)
		),
		'stats_only_record' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_courses.stats_only_record',
			'config' => array(
				'type' => 'check',
				'default' => 0
			)
		),
		'stats_reminder_sent' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_courses.stats_reminder_sent',
			'config' => array(
				'type' => 'input',
				'size' => 10,
				'eval' => 'datetime',
				'checkbox' => 1,
				'default' => time()
			),
		),
		'teacher' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_courses.teacher',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectMultipleSideBySide',
				'foreign_table' => 'fe_users',
				'MM' => 'tx_iscourses2_courses_teachers_mm',
				'size' => 10,
				'autoSizeMax' => 30,
				'maxitems' => 9999,
				'multiple' => 0,
				'wizards' => array(
					'_PADDING' => 1,
					'_VERTICAL' => 1,
					'edit' => array(
						'module' => array(
							'name' => 'wizard_edit',
						),
						'type' => 'popup',
						'title' => 'Edit',
						'icon' => 'edit2.gif',
						'popup_onlyOpenIfSelected' => 1,
						'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
						),
					'add' => Array(
						'module' => array(
							'name' => 'wizard_add',
						),
						'type' => 'script',
						'title' => 'Create new',
						'icon' => 'add.gif',
						'params' => array(
							'table' => 'fe_users',
							'pid' => '###CURRENT_PID###',
							'setValue' => 'prepend'
						),
					),
				),
			),
		),
		'owner' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:is_courses2/Resources/Private/Language/locallang_db.xlf:tx_iscourses2_domain_model_courses.owner',
			'config' => array(
				'type' => 'select',
				'renderType' => 'selectSingle',
				'foreign_table' => 'fe_users',
				'minitems' => 0,
				'maxitems' => 1,
			),
		),
		
	),
);