<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'INSOR.' . $_EXTKEY,
	'Courses',
	'Kurse'
);

$pluginSignature = str_replace('_','',$_EXTKEY) . '_courses';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/flexform_courses.xml');

if (TYPO3_MODE === 'BE') {

	/**
	 * Registers a Backend Module
	 */
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
		'INSOR.' . $_EXTKEY,
		'web',	 // Make module a submodule of 'web'
		'courses',	// Submodule key
		'',						// Position
		array(
			'Courses' => 'list, show, edit, update, delete, upload, stats, listStats, mailStats, downloadStats','Teachers' => 'list, show, edit, update',
		),
		array(
			'access' => 'user,group',
			'icon'   => 'EXT:' . $_EXTKEY . '/courses.png',
			'labels' => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_courses.xlf',
		)
	);

}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Generisches Kurse Anmeldesystem');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_iscourses2_domain_model_courses', 'EXT:is_courses2/Resources/Private/Language/locallang_csh_tx_iscourses2_domain_model_courses.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_iscourses2_domain_model_courses');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_iscourses2_domain_model_staticdata', 'EXT:is_courses2/Resources/Private/Language/locallang_csh_tx_iscourses2_domain_model_staticdata.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_iscourses2_domain_model_staticdata');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_iscourses2_domain_model_statistikfragebogenlink', 'EXT:is_courses2/Resources/Private/Language/locallang_csh_tx_iscourses2_domain_model_statistikfragebogenlink.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_iscourses2_domain_model_statistikfragebogenlink');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
    $_EXTKEY,
    'tx_iscourses2_domain_model_courses'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
    $_EXTKEY,
    'fe_users'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
    $_EXTKEY,
    'tx_iscourses2_domain_model_staticdata'
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder